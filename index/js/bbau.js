$(function() {
    $('#searchtxt').bind({
        focus: function() {
            $(".seoicn").css({
                "color": "#3ea1e2",
                "transition": "all 1.5s ease 0s"
            });
            var x = $(this).offset();
            $(".auto_hidden").css("left", x.left);
            $(".auto_hidden").css("top", x.top + 62)
        },
        blur: function() {
            $(".seoicn").css({
                "color": "#b0bec5",
                "transition": "all 1.5s ease 0s"
            });
            setTimeout(function() {
                $("#tauto").removeClass("auto_show");
                $("#tauto").addClass("auto_hidden")
            },
			200)
        }
    })
})
