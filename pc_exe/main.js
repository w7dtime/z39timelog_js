const electron = require('electron')
// Module to control application life.

// Module to create native browser window.
const {app, Menu, MenuItem, BrowserWindow , globalShortcut , screen , remote  } = electron;

const path = require('path')
const url = require('url')

let mainWindow
let width , height ;

function createWindow() {
  let size = screen.getPrimaryDisplay().workAreaSize
  width = parseInt(size.width)  
  height = parseInt(1080*size.width/1920+30)
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: width,
    height: height,
    // transparent: true,
    // show: false
  });
 // todo8.cn
  mainWindow.loadURL( 'https://todo8.cn/?action=electron' ) ;
  // mainWindow.loadURL( 'https://todo8.cn/?action=electron#/note' ) ;

  // globalShortcut.register('CommandOrControl+F', () => {
  // const FindInPage  = require('electron-find')
  //    let findInPage = new FindInPage(remote.getCurrentWebContents())
  //    findInPage.openFindWindow()
  // });

  // mainWindow.loadURL(url.format({
  //   pathname: path.join(__dirname, 'index.html'),
  //   protocol: 'file:',
  //   slashes: true
  // }))

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  mainWindow.on('closed', function () {
    mainWindow = null
    globalShortcut.unregisterAll();
    console.log("globalShortcut.unregisterAll();");
  })
  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });
  mainWindow.maximize();
  win_event(mainWindow)
}

function win_event(win){
    win.webContents.on('new-window' , ( event , url , fname, disposition , options )=>{
    let childWindow ;
    childWindow = new BrowserWindow({width: width -100 ,  height:height-100 , webPreferences: { nodeIntegration:false } })
    win_event(childWindow);
    childWindow.loadURL(url) ;
    // childWindow.maximize();
    event.preventDefault() ;
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// app.on('ready', createWindow) ;
app.on('ready', function () {

    createWindow(); 
    app.on('browser-window-focus', function () {
      console.log("browser-window-focus");
      globalShortcut.register("CommandOrControl+R", () => {
          mainWindow.reload(); // remote.getCurrentWindow()
      });
      globalShortcut.register("f5", () => {
          mainWindow.reload();  // mainWindow 
      });
    });
    app.on('browser-window-blur', function () {
      console.log("browser-window-blur");
      globalShortcut.unregisterAll()
    });
} )

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
