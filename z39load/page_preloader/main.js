/*
作品:姜禄建 杭州知达
qq:5526555
tel:0571-26288882
    4000076003
使用方法
var preloader ;
seajs.use("page_preloader/main",function(loader){
preloader = loader ;
loader.init();
loader.loadCb = function(){
   console.info("load 100%");
}
// loadCb.update(50);
});*/
define(function(require, exports, module) {
    // require('js/jquip.all.min.js');
    require('js/jquery.min.js');
    require('js/prefixfree.js');
    require('page_preloader/heart.min.css');

    var preloader_min = 1500,
        pre_begin = -1;
    var prepareCb;
    var loadCb; //回调function

    function init() {
        initData();
        initView();
        // console.info("preloader in!");    
    }

    function initData() {
        pre_begin = new Date().getTime();
    }

    function initView() {
        // var w = $(window).width();
        // var h = $(window).height();
        // $('body').width(w).height(h);
        $(".meter > span").width(0);
        StyleFix.process();

        $.get("page_preloader/main.html", "", function(data) {
            $("body").append(data);

            $(".footer").css("display", "block");
            $(".page_preloader main").css("display", "block");
            prepareIn();
            if (GetQueryString("debug")) {
                loadedComplete();
            }
        });
        // preload_progress(100);
    }
    // public
    function preload_progress(num) {
        try {
            $(".meter > span").each(function() {
                var t = $(this);
                var w = $(this).width() + 5;
                var timer = setInterval(function() {
                    var n = Math.ceil(($(this).width() + 5) / t.parent().width() * 100);
                    t.html(n + '%');
                    if (num > w) {
                        if (n >= num) {
                            t.html(num + '%');
                            clearInterval(timer);
                        }
                    } else {
                        if (n >= num) {
                            t.html(num + '%');
                            clearInterval(timer);
                        }
                    }

                }, 100);
                if (num < 100) {
                    $(this).animate({ width: num + '%' }, 1200);
                } else {
                    var uTime = new Date().getTime() - pre_begin;
                    var speed = uTime >= preloader_min ? 1200 : (1500 - uTime);
                    // console.info(speed);
                    $(this).animate({ width: num + '%' }, speed, "", loadedComplete);
                }
            });
            $("#start").css("width", num + '%');
        } catch (e) {
            console.warn("preload error: html not load");
        }
    }

    function loadedComplete() {
        $(".page_preloader").fadeOut(500, function() {
            $(".page_preloader").remove();
            $("body").css("overflow", "scroll");
        });
        drag_jinzhi = 1;
        if (loadCb) {
            loadCb();
        } else {
            console.warn("加载完成回调");
        }
    }

    function prepareIn() {
        // console.info("prepare in, " + preloader_min );
        if (prepareCb) {
            prepareCb();
        } else {
            try {
                preloader.prepareCb();
            } catch (e) {
                console.warn("预加载准备完成!");
            }
        }
    }

    function GetQueryString(name) {     
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");     
        var r = window.location.search.substr(1).match(reg);     
        if (r != null) return  unescape(r[2]);
        return null;
    }


    exports.update = preload_progress; //更新进度
    exports.init = init;
    exports.loadCb = loadCb;
    exports.prepareCb = prepareCb;
});