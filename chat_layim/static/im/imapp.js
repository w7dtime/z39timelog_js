if(!/^http(s*):\/\//.test(location.href)){
  alert('请部署到localhost上查看该演示');
}

document.title = userConfig.name;
//注册自定义消息
function registerMessage(type){
    var messageName = type; // 消息名称。
    var objectName = "s:" + type; // 消息内置名称，请按照此格式命名 *:* 。
    var mesasgeTag = new RongIMLib.MessageTag(true,true); //true true 保存且计数，false false 不保存不计数。
    var propertys = ["avatar","groupid","id", "name", "type", "username", "content"]; // 消息类中的属性名。
    RongIMClient.registerMessageType(messageName, objectName, mesasgeTag, propertys);
}

function startInit(layim){
    var params = rongConfig;
    var userId = "";
    var callbacks = {
        getInstance : function(instance){
            //RongIMLib.RongIMEmoji.init();
            //instance.sendMessage
            registerMessage("FriendMessage");
            registerMessage('GroupMessage');
        },
        getCurrentUser : function(userInfo){
            console.log(userInfo.userId);
            userId = userInfo.userId;
            console.log("链接成功；userid=" + userInfo.userId);
        },
        receiveNewMessage : function(message){
            //判断是否有 @ 自己的消息
            var mentionedInfo = message.content.mentionedInfo || {};
            var ids = mentionedInfo.userIdList || [];
            for(var i=0; i < ids.length; i++){
                if( ids[i] == userId){
                    alert("有人 @ 了你！");
                }
            }
            var msgdata = message.content;
            if (message.senderUserId != userConfig.userId) {
                layim.getMessage({
                  username: msgdata.username
                  ,avatar: msgdata.avatar
                  ,id: message.targetId
                  ,type: msgdata.type
                  ,content: msgdata.content
              });
            }
        }
    };
    init(params,callbacks);
}

layui.use(['jquery', 'layim'], function(){
  var $ = layui.jquery;
  var layim = layui.layim;
  window.layim = layim;
  startInit(layim);
  
  
  //基础配置
          layim.config({
            //初始化接口
            /*
            init: {
              url: 'json/getList.json'
              ,data: {}
            }*/
            
            //或采用以下方式初始化接口
            
            init: {
              mine: {
                "username": userConfig.name //我的昵称
                ,"id": userConfig.userId //我的ID
                ,"status": "online" //在线状态 online：在线、hide：隐身
                ,"remark": "我是" + userConfig.name //我的签名
                ,"avatar": "/layim/static/images/headimg/" + (userConfig.userId-100) + ".jpg" //我的头像
              }
              ,friend: [{
                  groupname: '分组1',
                  id: 1,
                  online: 1,
                  list:[]
                },{
                  groupname: '分组2',
                  id: 2,
                  online: 1,
                  list:[]
                },{
                  groupname: '分组3',
                  id: 3,
                  online: 1,
                  list:[]
                },

              ]
              ,group: [{
                avatar: "http://tva3.sinaimg.cn/crop.64.106.361.361.50/7181dbb3jw8evfbtem8edj20ci0dpq3a.jpg"
                ,groupname: '群组1'
                ,id: "g001",
                members: 2
              }]
            }
            
            

            //查看群员接口
            ,members: {
              url: 'json/g001.json'
              ,data: {
                "owner": {
                  "username": "user101"
                  ,"id": "101"
                  ,"avatar": "/layim/static/images/headimg/1.jpg"
                  ,"sign": "我是user101"
                },
                members: 2,
                list: [{
                  username: "user101"
                  ,id: '101'
                  ,avatar: "/layim/static/images/headimg/1.jpg"
                  ,sign: "我是user101"
                },{
                  username: "user102"
                  ,id: '102'
                  ,avatar: "/layim/static/images/headimg/2.jpg"
                  ,sign: "我是user102"
                }]
              }
            }
            
            //上传图片接口
            ,uploadImage: {
              url: '/upload/image' //（返回的数据格式见下文）
              ,type: '' //默认post
            } 
            
            //上传文件接口
            ,uploadFile: {
              url: '/upload/file' //（返回的数据格式见下文）
              ,type: '' //默认post
            }
            
            ,isAudio: true //开启聊天工具栏音频
            ,isVideo: true //开启聊天工具栏视频
            
            //扩展工具栏
            ,tool: [{
              alias: 'code'
              ,title: '代码'
              ,icon: '&#xe64e;'
            }]
            
            //,brief: true //是否简约模式（若开启则不显示主面板）
            
            //,title: 'WebIM' //自定义主面板最小化时的标题
            //,right: '100px' //主面板相对浏览器右侧距离
            //,minRight: '90px' //聊天面板最小化时相对浏览器右侧距离
            ,initSkin: '5.jpg' //1-5 设置初始背景
            //,skin: ['aaa.jpg'] //新增皮肤
            //,isfriend: false //是否开启好友
            //,isgroup: false //是否开启群组
            //,min: true //是否始终最小化主面板，默认false
            ,notice: true //是否开启桌面消息提醒，默认false
            //,voice: false //声音提醒，默认开启，声音文件为：default.mp3
            
            ,msgbox: layui.cache.dir + 'css/modules/layim/html/msgbox.html' //消息盒子页面地址，若不开启，剔除该项即可
            ,find: layui.cache.dir + 'css/modules/layim/html/find.html' //发现页面地址，若不开启，剔除该项即可
            ,chatLog: layui.cache.dir + 'css/modules/layim/html/chatLog.html' //聊天记录页面地址，若不开启，剔除该项即可
            
          });
    $.get('/layim/server/friends.php', {userId: userConfig.userId}, function(data,status){
        console.log(data);
        console.log(userConfig.userId);
        if (data.errno == 0) {
            var friendList = data.data;
            var groupList = [];
            for(var i=0; i<friendList.length; i++) {
                var friend = friendList[i];
                console.log(friend);
                if (groupList.indexOf(friend.gid) < 0) {
                    /*layim.addList({
                        type: 'friend',
                        groupname: friend.gname,
                        id: friend.gid,
                        list: []
                    });*/
                    groupList.push(friend.gid);
                }
                console.log("OK");
                layim.addList({
                    type: 'friend',
                    avatar: friend.headimgurl,
                    username: friend.nname,
                    groupid: friend.gid,
                    id: friend.uid,
                    remark: "我是" + friend.nname
                });
            }
        }
    });


  /*
  layim.chat({
    name: '在线客服-小苍'
    ,type: 'kefu'
    ,avatar: 'http://tva3.sinaimg.cn/crop.0.0.180.180.180/7f5f6861jw1e8qgp5bmzyj2050050aa8.jpg'
    ,id: -1
  });
  layim.chat({
    name: '在线客服-心心'
    ,type: 'kefu'
    ,avatar: 'http://tva1.sinaimg.cn/crop.219.144.555.555.180/0068iARejw8esk724mra6j30rs0rstap.jpg'
    ,id: -2
  });
  layim.setChatMin();*/

  //监听在线状态的切换事件
  layim.on('online', function(data){
    //console.log(data);
  });
  
  //监听签名修改
  layim.on('sign', function(value){
    //console.log(value);
  });

  //监听自定义工具栏点击，以添加代码为例
  layim.on('tool(code)', function(insert){
    layer.prompt({
      title: '插入代码'
      ,formType: 2
      ,shade: 0
    }, function(text, index){
      layer.close(index);
      insert('[pre class=layui-code]' + text + '[/pre]'); //将内容插入到编辑器
    });
  });

  //监听layim建立就绪
  layim.on('ready', function(res){
    console.log('layim ready');
    //console.log(res.mine);
    
  RongIMLib.RongIMClient.setOnReceiveMessageListener({
      // 接收到的消息
      onReceived: function (message) {
          // 判断消息类型
          console.log("新消息: " + message.targetId);
          console.log(message);
          //callbacks.receiveNewMessage && callbacks.receiveNewMessage(message);
      }
  });

  /*
  RongIMClient.getInstance().hasRemoteUnreadMessages(token,{
      onSuccess:function(hasMessage){
          if(hasMessage){
              // 有未读的消息
              console.log("有未读的消息");
          }else{
              // 没有未读的消息
              console.log("没有未读的消息");
          }
      },onError:function(err){
          // 错误处理...
          console.log("获取未读消息失败");
      }
  });*/
  
    //layim.msgbox(5); //模拟消息盒子有新消息，实际使用时，一般是动态获得
  
    //添加好友（如果检测到该socket）
    /*layim.addList({
      type: 'group'
      ,avatar: "http://tva3.sinaimg.cn/crop.64.106.361.361.50/7181dbb3jw8evfbtem8edj20ci0dpq3a.jpg"
      ,groupname: 'Angular开发'
      ,id: "12333333"
      ,members: 0
    });
    layim.addList({
      type: 'friend'
      ,avatar: "http://tp2.sinaimg.cn/2386568184/180/40050524279/0"
      ,username: '冲田杏梨'
      ,groupid: 2
      ,id: "1233333312121212"
      ,remark: "本人冲田杏梨将结束AV女优的工作"
    });*/
    
    setTimeout(function(){
      //接受消息（如果检测到该socket）
      layim.getMessage({
        username: "Hi"
        ,avatar: "http://qzapp.qlogo.cn/qzapp/100280987/56ADC83E78CEC046F8DF2C5D0DD63CDE/100"
        ,id: "10000111"
        ,type: "friend"
        ,content: "临时："+ new Date().getTime()
      });
      
      /*layim.getMessage({
        username: "贤心"
        ,avatar: "http://tp1.sinaimg.cn/1571889140/180/40030060651/1"
        ,id: "100001"
        ,type: "friend"
        ,content: "嗨，你好！欢迎体验LayIM。演示标记："+ new Date().getTime()
      });*/
      
    }, 3000000000);
  });

  //监听发送消息
  layim.on('sendMessage', function(data){
    var To = data.to;
    console.log(data);
    if(To.type === 'friend'){
      //layim.setChatStatus('<span style="color:#FF5722;">对方正在输入。。。</span>');
      var msg = new RongIMLib.RongIMClient.RegisterMessage.FriendMessage({
        avatar: data.mine.avatar,
        groupid: To.groupid,
        id: userConfig.userId,
        username: userConfig.name,
        name: To.name,
        type: 'friend',
        content: data.mine.content
      });
      RongIMLib.RongIMClient.getInstance().sendMessage(RongIMLib.ConversationType.PRIVATE,To.id.toString(), msg, {
          onSuccess: function (message) {
            console.log(data.mine.content, " 发送成功");
          },
          onError: function (errorCode) {
            console.log(data.mine.content, " 发送失败");
          }
      });
    } else if (To.type === 'group') {
      var msg = new RongIMLib.RongIMClient.RegisterMessage.GroupMessage({
        avatar: data.mine.avatar,
        groupid: To.id,
        id: userConfig.userId,
        username: userConfig.name,
        name: To.name,
        type: To.type,
        content: data.mine.content
      });
      RongIMLib.RongIMClient.getInstance().sendMessage(RongIMLib.ConversationType.GROUP,To.id.toString(), msg, {
          onSuccess: function (message) {
            console.log(data.mine.content, " 群消息发送成功");
          },
          onError: function (errorCode) {
            console.log(data.mine.content, " 群消息发送失败");
            console.log(errorCode);
          }
      });
    } else {
      console.log("other message");
    }
    
    //演示自动回复
    /**setTimeout(function(){
      var obj = {};
      if(To.type === 'group'){
        obj = {
          username: '模拟群员'+(Math.random()*100|0)
          ,avatar: layui.cache.dir + 'images/face/'+ (Math.random()*72|0) + '.gif'
          ,id: To.id
          ,type: To.type
          ,content: autoReplay[Math.random()*9|0]
        }
      } else {
        obj = {
          username: To.name
          ,avatar: To.avatar
          ,id: To.id
          ,type: To.type
          ,content: autoReplay[Math.random()*9|0]
        }
        layim.setChatStatus('<span style="color:#FF5722;">在线</span>');
      }
      layim.getMessage(obj);
    }, 1000000000);**/
  });

  //监听查看群员
  layim.on('members', function(data){
    //console.log(data);
  });
  
  //监听聊天窗口的切换
  layim.on('chatChange', function(res){
    var type = res.data.type;
    console.log(res.data.id)
    if(type === 'friend'){
      //模拟标注好友状态
      //layim.setChatStatus('<span style="color:#FF5722;">在线</span>');
    } else if(type === 'group'){
      //模拟系统消息
      layim.getMessage({
        system: true
        ,id: res.data.id
        ,type: "group"
        ,content: '模拟群员'+(Math.random()*100|0) + '加入群聊'
      });
    }
  });
});