var phonesApp = phonesApp || [];

// 通过dcloud 调用app通讯录
function getAppPhone() {
  plus.contacts.getAddressBook(plus.contacts.ADDRESSBOOK_SIM, function(addressbook) {
    addressbook.find(["displayName", "phoneNumbers"], function(contacts) {
      console.log("ADDRESSBOOK_SIM");
      // console.log(JSON.stringify(contacts));
      length = contacts.length;
      var contactStr = '';
      var tempArr = [];
      for (var i = 0; i < length; i++) {
        try {
          tempArr.push({
            name: contacts[i].displayName,
            phone: contacts[i].phoneNumbers[0].value.replace(/[- \+]/ig, ""),
            camelChars: pinyin.getCamelChars(contacts[i].displayName).toUpperCase(),
            fullChars: pinyin.getFullChars(contacts[i].displayName)
          });
        } catch (e) {
          continue;
        }
      }
      phonesApp = _.sortBy(tempArr, 'fullChars');
      try {
        app.$store.commit({ type: "z43/setPhonesApp", phones: phonesApp });
      } catch (e) {
        console.log("vue maybe not ready！");
      }
    });
  }, function(e) {
		var message = "从通讯录添加项目成员,待小伙伴一起飞";
		var title = "开通通讯录权限";
		plus.nativeUI.alert(message, function() {}, title);
  });
}
