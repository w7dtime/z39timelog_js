		var firstIn = true;

		var speedSlow = 3000,
		    speedFast = 1000,
		    speedUse = 1000;
		var offsetTime = 100; //定时器容错率
		var actionDownTime = 10 * 1000; //2*60*1000 ;  //静止2分钟,动作结束.

		var moveDiff = 3; //抖动范围

		var logStr = "";
		var lastPosTime = 0;
		var orientationId = null;
		var postionId = null;
		var lastOrientation;
		var lastChangeTime = 0,
		    spaceTime = 5000,
		    lastActionTime = 0;
		var actionIn = false;

		var isDebug = false; // true ;
		function initApp() {


		}

		// try{ if(isApp)runWatch() }catch(e){ console.log("app not ready") };
		function runWatch() {
		    if (postionId && orientationId) return console.log("位置监听已经初始化成功.");
		    watchPosition();
		    watchOrientation();
		}

		function watchOrientation() {
		    orientationWatchStop();
		    //方向
		    orientationId = plus.orientation.watchOrientation(orientationChange, function(e) {
		        //alert("Orientation error: " + e.message);				
				if (lastTimeRunPos == 0 && lastTimeRunOrie ==0) noticeOpen();
		    }, {
		        frequency: 500
		    });
		}
		var lastTimeRunOrie = 0;
		var actionArr = [],
		    actionItem, moveCount = 0;
		var todayStr;
		var now;

		function orientationChange(o) {
		    now = moment();
		    if (now - lastTimeRunOrie < speedUse - offsetTime) {
		        //				console.log("太短");
		        return;
		    }
		    lastTimeRunOrie = now;

		    var ori = {};
		    ori.alpha = parseInt(o.alpha);
		    ori.beta = parseInt(o.beta);
		    ori.gamma = parseInt(o.gamma);
		    //			ori.magneticHeading = parseInt(o.magneticHeading) ;
		    ori.trueHeading = parseInt(o.trueHeading);
		    //			ori.headingAccuracy = parseInt(o.headingAccuracy) ;

		    var x = ori.alpha;
		    var y = ori.beta;
		    var z = ori.gamma;
		    var dir = ori.trueHeading;

		    var orienData = { ox: x, oy: y, oz: z, dir: dir };

		    var moveDiff = 0;
		    if (lastOrientation) {
		        moveDiff = isObjectValueDiff(lastOrientation, ori);
		    }
		    /*var tempDayStr = now.format('YYYY-MM-DD');
			if( todayStr && todayStr != tempDayStr ){  //过了24点,新的一天
				if( actionIn ){
					actionItem.end = new moment(tempDayStr)  ;
					actionItem.state = "run";
					actionArr.push( actionItem ) ;	
					console.log("new day");
					logAction(actionItem) ;	
					lastChangeTime = actionItem.end  ;
					//第二天的动作直接添加、接上
					actionItem = {} ;
					actionItem.start = lastChangeTime ;
					actionItem.flags = [] ;
					actionItem.flags.push("隔日");
					refreshTime( actionItem );
				}else{					
//					addActionStatic( true ) ;   //已删除.
				}
				doWithNewDay() ;
				return ;
			}
			todayStr = tempDayStr ;
			console.log( todayStr );*/
		    if (lastOrientation && !moveDiff) { //如何和上次数据没变化 			
		        orienData.moveDiff = moveDiff;
		        putMoveData(moment().format('YYYY-MM-DD HH:mm:ss'), orienData, 2);
		        if (now - lastChangeTime >= spaceTime) {
		            //					speedUse = speedSlow ;
		        }
		        if (now - lastChangeTime >= 10000) { //10秒内清空抖动数据
		            moveCount = 0;
		        }
		        if (now - lastChangeTime >= actionDownTime && actionIn) { //动作结束					
		            actionNew(Action_Static);
		        }
		        refreshTime(actionItem);
		        //				console.log("action:" + actionIn + JSON.stringify( actionItem ) );
		        //				console.log("same: "+ moment().format('HH:mm:ss') + JSON.stringify( ori )) ;
		        return;
		    }
		    moveCount++;
		    //(now - appStartTime < 10000 ) 启动app时激活动作.
		    if ((moveCount >= 5 && !actionIn) || firstIn) {
		        firstIn = false;
		        console.log("Action_Move in ");
		        actionNew(Action_Move);
		    }
		    if (actionItem) {
		        actionItem.moveDiff += moveDiff;
		    }
		    //			console.log( moment().format('HH:mm:ss') + JSON.stringify( ori )) ;
		    //			console.log( moment().format('HH:mm:ss') + JSON.stringify( lastOrientation )) ;

		    lastChangeTime = now;
		    lastOrientation = ori;

		    refreshTime(actionItem);
		}
		var Action_Static = 1; // "Action_Static" ,
		var Action_Move = 2; //"Action_Move" ;   //静止动作  和 移动使用动作.
		function actionNew(type, isnew) {
		    //			console.log("logAction actionDefault: "+  JSON.stringify( actionDefault ) );
		    type = type || Action_Move; //默认运动使用.
		    //结束当前动作&添加到动作列表
		    if (actionItem) {
		        actionItem.end = (lastChangeTime == actionItem.start) ? now : lastChangeTime;
		        actionArr.push(actionItem);
		        console.log("action done" + JSON.stringify(actionItem));
		        if (actionItem.typea == Action_Static) {
		            var past = actionItem.end.format("x") - actionItem.start.format("x");
		            if (past >= 2000) { //大于2秒才记录,数值太小的忽略不管.
		                logAction(actionItem, 1);
		                actionItem.img = "static/z39h5/img/sit.png";
		            }
		        } else {
		            logAction(actionItem);
		        }
		        moveCount = 0;
		        lastChangeTime = now;
		    }
		    var lastAddr = "";
		    if (actionItem && actionItem.addr) {
		        lastAddr = actionItem.addr;
		    }
		    if (type == Action_Static) {
		        if (nowPosition && nowPosition.px && nowPosition.py) {
		            lastPosData = nowPosition;
		        }
		    }

		    //创建新动作
		    actionItem = Object.assign({}, actionDefault);
		    actionItem.timeday = moment().format("YYYY-MM-DD");
		    actionItem.start = now;
		    actionItem.addr = lastAddr;
		    if (isnew) {
		        actionItem.new = true;
		    }
		    if (type == Action_Move) {
		        actionIn = true; //动作开始			
		        actionItem.typea = Action_Move;
		    } else {
		        actionIn = false;
		        actionItem.title = "静止状态";
		        actionItem.typea = Action_Static;
		    }
		}
		//		index img title start end addr time flags
		var liItemStr = '<li class="mui-table-view-cell mui-checkbox mui-media"><a href="#userAction" data-number="{0}" data-vote="{6}"><img class="mui-media-object mui-pull-left" src="{1}"><div class="mui-media-body"><span class="tlog_title">{2}</span>{7}<p class="mui-ellipsis">{3}-{4}  {5}</p></div></a></li>';
		var flagitemStr = '<span class="tlog_flag">{0}</span>';
		var flagIngitemStr = '<span class="tlog_flag tlog_flag_g">{0}</span>';

		function logAction(item, area) { //打印动作数据			
		    if (item.state == "run") {
		        //				tem.flags.push("隔日");	
		        addFlag("隔日");
		    }
		    var clockTime = parseInt(item.end.format('H'));
		    var defaultImg = getTimeImg(clockTime);
		    defaultImg = getActionImg(item) || defaultImg;
		    item.img = defaultImg;
		    if (item.state == 1) {
		        item.img = "static/z39h5/img/sit.png";
		    }
		    //			console.log("logAction:"+  JSON.stringify( item ) );
		    var len = actionArr.length,
		        actionlen = 0;
		    for (var i = 0; i <= len - 1; i++) {
		        var action = actionArr[i];
		        if (action.typea == Action_Move) {
		            actionlen++;
		        }
		    }
		    var index = item.typea == Action_Move ? actionlen : (len - actionlen);
		    var img = item.img;
		    item.title = (item.title || "动作") + index;
		    var start = item.start.format('HH:mm:ss');
		    var end = item.end.format('HH:mm:ss'); // item.end.format('HH:mm:ss') ;
		    var addr = item.addr; // || "[公司]互联网金融大厦" ; 
		    var time = Math.floor((item.end - item.start) / 1000).formatTime();

		    item.duration = time;
		    send2vue(item, 1);

		    // showActionItem(item , area) ;
		    lastActionTime = item.end;
		}

		// html相关界面显示
		function showActionItem(item, area) {
		    var flags = "";
		    flags = getFlagStr(item);
		    var tempStr = liItemStr.formatStr(index, img, title, start, end, addr, time, flags);
		    area = area || 2;
		    if (area == 2) {
		        jQuery(".output2 li").eq(0).after(tempStr);
		    } else {
		        jQuery(".output1 li").eq(0).after(tempStr);
		    }
		}

		function getActionImg(item) {
		    var imgUlr = "";
		    if (item.speed > 0) {
		        imgUlr = "static/z39h5/img/move.png";
		    }
		    return imgUlr;
		}
		var actionDefault = { title: "动作", img: "static/z39h5/img/logo.png", flags: [], addr: "", stepCount: 0, speed: 0 };

		function refreshTime(item) {
		    if (item == null) {
		        console.warn("refreshTime item null");
		        return;
		    }
		    if (item.state == "run") {
		        taddFlag("隔日");
		    }
		    if (item.typea == Action_Static) {
		        item.title = "静止中";
		    }

		    var index = actionArr.length;
		    var img = "static/z39h5/img/time.png";
		    var title = item.title || "动作" + index;
		    var start = item.start.format('HH:mm:ss');
		    var end = "[now]"; // item.end.format('HH:mm:ss') ; 
		    var addr = item.addr || "【无】";
		    var time = Math.floor((moment() - item.start) / 1000).formatTime();
		    item.duration = time;
		    item.dis = getActionDis();
		    send2vue(item, 0);

		    // refreshTimeShow( item ) ;
		}
		//数据传递给vue.  type: 0 1  0:actionCurr 1:actionadd
		function send2vue(item, type) {
		    if (nowPosition) {
		        item.px = nowPosition.px;
		        item.py = nowPosition.py;
		    }
		    var data = {};
		    try {
		        if (type == 1) {
		            data = Object.assign({ type: "z39/actionAdd" }, item);
		            data.start = data.start ? data.start.format('HH:mm:ss') : '';
		            data.end = data.end ? data.end.format('HH:mm:ss') : '';
		            app.$store.commit(data);
		            console.log("item", JSON.stringify(data));
		        } else {
		            data = Object.assign({ type: "z39/actionCurr" }, item);
		            data.start = data.start ? data.start.format('HH:mm:ss') : '';
		            data.end = data.end ? data.end.format('HH:mm:ss') : '';
		            app.$store.commit(data);
		        }
		    } catch (e) {
		        console.log("vue maybe not ready！");
		    }
		}
		// html相关界面显示
		function refreshTimeShow(item) {
		    var flags = "";
		    flags = getFlagStr(item);
		    flags += flagIngitemStr.formatStr("进行中");
		    var tempStr = liItemStr.formatStr(index, img, title, start, end, addr, time, flags);
		    jQuery(".output2 li").eq(0).prop('outerHTML', tempStr);
		}
		// html相关界面显示
		function getFlagStr(item) {
		    var flags = "";
		    for (var i = 0; i < item.flags.length; i++) {
		        flags += flagitemStr.formatStr(item.flags[i]);
		    }
		    if (item.speed > 0) {
		        flags += flagitemStr.formatStr(item.speed + "km/h");
		    } else {
		        flags += flagitemStr.formatStr("原地");
		    }
		    if (item.stepCount >= 10) {
		        if (item.stepCount < 1000) {
		            flags += flagitemStr.formatStr(item.stepCount + "步");
		        } else {
		            flags += flagitemStr.formatStr(Math.floor(item.stepCount / 100) / 10 + "k步");
		        }
		    }
		    if (lastPosData && nowPosition && lastPosData != nowPosition) {
		        //				console.log(JSON.stringify(lastPosData));
		        //				console.log(JSON.stringify(nowPosition));				
		        var dis = getFlatternDistance(lastPosData.px, lastPosData.py, nowPosition.px, nowPosition.py);
		        dis = Math.ceil(dis);
		        if (!isNaN(dis)) {
		            item.dis = dis;
		            flags += flagitemStr.formatStr("" + dis + "m");
		        }
		    }
		    return flags;
		}

		//获取 vue中需要使用的flagtop标签字符串.
		function getFlagVueStr(item) {
		    var tempFlags = [];
		    for (var i = 0; i < item.flags.length; i++) { //基本中文
		        tempFlags.push(item.flags[i])
		    }
		    if (item.speed > 0) {
		        tempFlags.push(item.speed + "km/h")
		    } else {
		        tempFlags.push("原地");
		    }
		    if (item.stepCount >= 10) {
		        if (item.stepCount < 1000) {
		            tempFlags.push(item.stepCount + "步");
		        } else {
		            tempFlags.push(Math.floor(item.stepCount / 100) / 10 + "k步");
		        }
		    }
		    if (lastPosData && nowPosition && lastPosData != nowPosition) {
		        var dis = getFlatternDistance(lastPosData.px, lastPosData.py, nowPosition.px, nowPosition.py);
		        dis = Math.ceil(dis);
		        if (!isNaN(dis)) {
		            item.dis = dis;
		            tempFlags.push("" + dis + "m");
		        }
		    }
		    return tempFlags.join(",");
		}

		//      获取当前行动距离上1行为的dis
		function getActionDis() {
		    var dis = 0;
		    if (lastPosData && nowPosition && lastPosData != nowPosition) {
		        dis = getFlatternDistance(lastPosData.px, lastPosData.py, nowPosition.px, nowPosition.py);
		        dis = Math.ceil(dis);
		        if (isNaN(dis)) {
		            dis = 0;
		        }
		    }
		    return dis;
		}

		var moveDataArr = [],
		    moveDataItem;

		function getMoveData(timeKey) { //timeKey 值为"2017-04-12 23:47:04" ,是 moment().format('YYYY-MM-DD HH:mm:ss'); 的结果 
		    var item = moveDataArr[timeKey];
		    if (item == undefined) {
		        item = {};
		        item.time = timeKey;
		        moveDataArr[timeKey] = item;
		    }
		    return item;
		}
		var lastPosData, lastOriData;
		// 值的数据类型type 值为1 2 ,  1:postion 2: orientation
		function putMoveData(timeKey, data, type) {
		    type = type || 1; //默认为1
		    var diff = isPosValueDiff(lastPosData, data);
		    if (type == 1) { //postion
		        if (diff < 5 && lastPosData != null) {
		            return;
		        }
		    }
		    var item = getMoveData(timeKey);
		    item = Object.assign(item, data);
		    if (type == 1) { //postion
		        lastPosData = data;
		    } else {
		        lastOriData = data;
		    }
		    //如果数据无变化需要删除
		}

		function doWithNewDay() {
		    //需要完善,数据需要传输服务器.
		    console.error("doWithNewDay error in!");
		    //			moveDataArr = [] ;
		    //			actionArr = [] ;
		}

		function noticeOpen() {
		    if (plus.os.name == "Android") {
		        var context = plus.android.importClass("android.content.Context");
		        var locationManager = plus.android.importClass("android.location.LocationManager");
		        var main = plus.android.runtimeMainActivity();
		        var mainSvr = main.getSystemService(context.LOCATION_SERVICE);
		        var gpsProvider = mainSvr.isProviderEnabled(locationManager.GPS_PROVIDER); //检查是否开启了GPS
		        if (!gpsProvider) {
		            var message = "执行任务时,提醒您不要玩手机.通过手机位置记录行为";
		            var title = "功能需要开启GPS";
		            var alertCB = function() {
		                var Intent = plus.android.importClass("android.content.Intent");
		                var mIntent = new Intent('android.settings.LOCATION_SOURCE_SETTINGS');
		                main.startActivity(mIntent); //打开GPS设置
		            }
		            plus.nativeUI.alert(message, alertCB, title);
		        }
		    } else {
		        var message = "执行任务时,提醒您不要玩手机.通过手机位置记录行为.\n定位服务未开启，请进入系统［设置］> [隐私] > [定位服务]中打开开关，并允许使用定位服务";
		        var title = "功能需要开启GPS";
		        plus.nativeUI.alert(message, function() {}, title);
		    }
		}

		function watchPosition() {
		    postionWatchStop();
		    //定位
		    postionId = plus.geolocation.watchPosition(positonChange, function(e) {
		        // alert("Geolocation error: " + e.message);
				if (lastTimeRunPos == 0 && lastTimeRunOrie ==0) noticeOpen();		        
		    }, {
		        maximumAge: 500
		        //provider:'gaode'
		    });
		}
		var lastTimeflag = 0;
		var lastTimeRunPos = 0;

		function positonChange(position) {
		    var now = moment();
		    if (now - lastTimeRunPos < speedUse - offsetTime) {
		        return;
		    }
		    lastTimeRunPos = now;

		    var str = "";
		    str += "地址：" + position.addresses + "\n"; //获取地址信息
		    str += "坐标类型：" + position.coordsType + "\n";
		    var timeflag = position.timestamp; //获取到地理位置信息的时间戳；一个毫秒数；
		    str += "时间戳：" + timeflag + "\n";
		    var codns = position.coords; //获取地理坐标信息；
		    var lat = codns.latitude; //获取到当前位置的纬度；
		    str += "纬度：" + lat + "\n";
		    var longt = codns.longitude; //获取到当前位置的经度
		    str += "经度：" + longt + "\n";
		    var alt = codns.altitude; //获取到当前位置的海拔信息；
		    str += "海拔：" + alt + "\n";
		    var accu = codns.accuracy; //地理坐标信息精确度信息；
		    str += "精确度：" + accu + "\n";
		    var altAcc = codns.altitudeAccuracy; //获取海拔信息的精确度；
		    str += "海拔精确度：" + altAcc + "\n";
		    var head = codns.heading; //获取设备的移动方向 ；
		    str += "移动方向：" + head + "\n";
		    var sped = codns.speed; //获取设备的移动速度 ；
		    str += "移动速度：" + sped;

		    sped = Math.floor(sped * 10) / 10;
		    var posData = { px: longt, py: lat, pz: alt, speed: sped, addr: position.addresses };

		    if (actionItem) {
		        if (sped > 0 && getActionDis() > 0) { //判断条件还需要优化. 
		            actionItem.speed = Math.max(sped, actionItem.speed);
		        }
		        if (sped > 0 || !actionItem.addr) { //第一次或者 速度>0时可以设置地址
		            actionItem.addr = position.addresses;
		        }
		    }
		    if (lastPosData == null) { //2处地方设置lastPosData
		        lastPosData = posData;
		    }
		    nowPosition = posData;
		    var pinfo = JSON.stringify(posData);
		    //				console.log( moment().format('HH:mm:ss') + pinfo );
		    if (isDebug) {
		        jQuery(".output2 li:last-child").html(JSON.stringify(nowPosition));
		        if (lastPosData) {
		            var len = jQuery(".output2 li").length;
		            jQuery(".output2 li").eq(len - 2).html(JSON.stringify(lastPosData));
		        }
		    }
		    putMoveData(moment().format('YYYY-MM-DD HH:mm:ss'), posData, 1);
		    var time = moment().format('HH:mm:ss');
		    lastPosTime = now;
		    logStr += time + " " + parseInt(sped) + " " + parseInt(head);
		}
		var nowPosition; //当前经纬度位置坐标	

		jQuery("#content").on('tap', ".output2 li a", function(event) {
		    if (event.target.typea == "checkbox") {
		        console.log("d7game");
		        return;
		    }
		    var tar = jQuery(this);
		    target = tar;
		    userPicker.show(function(items) {
		        //				console.info(JSON.stringify(items[0]) );
		        if (items[0].value == "custom") {
		            var btnArray = ['确定', '取消'];
		            mui.prompt('请输入自定义名称：', '名称', '时间清单', btnArray, function(e) {
		                if (e.index == 0) {
		                    tar.find(".tlog_title").html(e.value);
		                    tar.find(".tlog_flag").addClass("tlog_flag_y");
		                } else {
		                    info.innerText = '你点了取消按钮';
		                }
		            })
		            return;
		        }
		        tar.find(".tlog_title").html(items[0].text);
		        tar.find(".tlog_flag").addClass("tlog_flag_y");
		    });
		});
		//获取时间对应的图片图标
		function getTimeImg(clockTime) {
		    var tempImg = "static/z39h5/img/logo.png";
		    switch (clockTime) {
		        case 23:
		        case 0:
		        case 1:
		        case 2:
		        case 3:
		        case 4:
		        case 5:
		        case 6:
		            tempImg = "static/z39h5/img/light2.png";
		            break;
		        case 8:
		        case 9:
		        case 10:
		        case 13:
		        case 14:
		        case 15:
		        case 16:
		            tempImg = "static/z39h5/img/sun.png";
		            break;
		        case 11:
		        case 12:
		            tempImg = "static/z39h5/img/tea.png";
		            break;
		        case 17:
		        case 18:
		        case 19:
		        case 20:
		        case 21:
		        case 22:
		            tempImg = "static/z39h5/img/light.png";
		            break;
		        default:
		            tempImg = "static/z39h5/img/logo.png";
		            break;
		    }
		    return tempImg;
		}

		function isObjectValueDiff(a, b) {
		    var aProps = Object.getOwnPropertyNames(a);
		    var bProps = Object.getOwnPropertyNames(b);

		    if (aProps.length != bProps.length) {
		        return false;
		    }
		    var diff = 0;

		    for (var i = 0; i < aProps.length; i++) {
		        var propName = aProps[i];

		        if (a[propName] !== b[propName]) {
		            diff += Math.abs(a[propName] - b[propName]);
		        }
		        if (diff > moveDiff) {
		            return diff;
		        }
		    }
		    return 0;
		}

		function isPosValueDiff(a, b) {
		    if (a == null || b == null) {
		        return 0;
		    }
		    var aProps = Object.getOwnPropertyNames(a);
		    var bProps = Object.getOwnPropertyNames(b);

		    if (aProps.length != bProps.length) {
		        return false;
		    }
		    var diff = 0;

		    for (var i = 0; i < aProps.length; i++) {
		        var propName = aProps[i];

		        if (a[propName] !== b[propName]) {
		            diff += Math.abs(a[propName] - b[propName]);
		        }
		        if (diff > 0.00005) {
		            return diff;
		        }
		    }
		    return 0;
		}

		function orientationWatchStop() {
		    if (orientationId) {
		        plus.orientation.clearWatch(orientationId);
		        orientationId = null;
		    } else {
		        console.log("停止监听 方向 变化");
		    }
		}

		var p = null;
		var iLast = 2;
		var SHAKE_THRESHOLD = 600;
		var last_update = 0;
		var sx, sy, sz, last_x = 0,
		    last_y = 0,
		    last_z = 0;
		var shakeCount = 0,
		    shakeLast = 0,
		    lastVector;
		var orX_last, orY_last, orZ_last;
		var stepCount = 0;
		//摇一摇判断逻辑
		function shake(a) {
		    var curTime = new Date().getTime();
		    if ((curTime - last_update) > 100) {
		        var diffTime = curTime - last_update;
		        last_update = curTime;
		        sx = a.xAxis;
		        sy = a.yAxis;
		        sz = a.zAxis;

		        var orX = sx - last_x > 0 ? 1 : 0;
		        var orY = sy - last_y > 0 ? 1 : 0;
		        var orZ = sz - last_z > 0 ? 1 : 0;
		        var dir = (sx + sy + sz - last_x - last_y - last_z) / diffTime * 10000;
		        var speed = Math.abs(dir);
		        //	            console.log("s:" + dir );
		        if (!p && speed > SHAKE_THRESHOLD) {
		            //	            	console.log("s:" + dir );
		            if (curTime - shakeLast > 320) {
		                shakeCount = 0;
		            }
		            shakeLast = curTime;
		            if ((orX_last ^ orX) + (orY_last ^ orY) + (orZ_last ^ orZ) >= 2) { //当x y z 2个方向的值改变时,判断为一次动作的变向.  
		                shakeCount++;
		                if(actionItem)actionItem.stepCount++;
		                console.log("变向成功： " + shakeCount); //也可以初略记录为走一步.
		            }
		            orX_last = orX;
		            orY_last = orY;
		            orZ_last = orZ;
		            if (shakeCount < 4) {
		                return;
		            }
		            //	            	mui.alert("大概走了步数:" + stepCount );
		            //	            	stepCount = 0 ;

		            shakeCount = 0;
		            actionNew(Action_Move, true);
		            addFlag("新建");

		            plus.device.vibrate(1000);
		            // Play audio
		            p = plus.audio.createPlayer('_www/audio/shake.wav');
		            p.play();
		            console.log("SHAKE in! ");
		            setTimeout(function() {
		                // Stop play audio
		                p.stop();
		                delete p;
		                p = null;
		            }, 2000);
		        }
		        last_x = sx;
		        last_y = sy;
		        last_z = sz;

		    }
		}

		function addFlag(arr) {
		    var item = actionItem;
		    arr = arr instanceof Array ? arr : [arr];
		    var flag;
		    //			for (var i = 0; i < arr.length; i++) {
		    //				flag = arr[i];
		    //				if( flag.indexOf("km/h") ){
		    //					
		    //				}
		    //			}
		    item.flags = item.flags.concat(arr);
		    //			console.log(item.flags);
		    return item;
		}

		function postionWatchStop() {
		    if (postionId) {
		        plus.geolocation.clearWatch(postionId);
		        postionId = null;
		    } else {
		        console.log("停止监听 位置 变化");
		    }
		}
		var EARTH_RADIUS = 6378137.0; //单位M
		var PI = Math.PI;

		function getRad(d) {
		    return d * PI / 180.0;
		}

		/**
		 * caculate the great circle distance
		 * @param {Object} lat1
		 * @param {Object} lng1
		 * @param {Object} lat2
		 * @param {Object} lng2
		 */
		function getGreatCircleDistance(lat1, lng1, lat2, lng2) {
		    var radLat1 = getRad(lat1);
		    var radLat2 = getRad(lat2);

		    var a = radLat1 - radLat2;
		    var b = getRad(lng1) - getRad(lng2);

		    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
		    s = s * EARTH_RADIUS;
		    s = Math.round(s * 10000) / 10000.0;

		    return s;
		}
		/**
		 * approx distance between two points on earth ellipsoid
		 * @param {Object} lat1
		 * @param {Object} lng1
		 * @param {Object} lat2
		 * @param {Object} lng2
		 */
		function getFlatternDistance(lat1, lng1, lat2, lng2) {
		    var f = getRad((lat1 + lat2) / 2);
		    var g = getRad((lat1 - lat2) / 2);
		    var l = getRad((lng1 - lng2) / 2);

		    var sg = Math.sin(g);
		    var sl = Math.sin(l);
		    var sf = Math.sin(f);

		    var s, c, w, r, d, h1, h2;
		    var a = EARTH_RADIUS;
		    var fl = 1 / 298.257;

		    sg = sg * sg;
		    sl = sl * sl;
		    sf = sf * sf;

		    s = sg * (1 - sl) + (1 - sf) * sl;
		    c = (1 - sg) * (1 - sl) + sf * sl;

		    w = Math.atan(Math.sqrt(s / c));
		    r = Math.sqrt(s * c) / w;
		    d = 2 * w * a;
		    h1 = (3 * r - 1) / 2 / c;
		    h2 = (3 * r + 1) / 2 / s;

		    return d * (1 + fl * (h1 * sf * (1 - sg) - h2 * (1 - sf) * sg));
		}


		// "http://{0}/{1}".format("www.songyanjun.net", "index.html")
		String.prototype.formatStr = function() {
		    if (arguments.length == 0) return this;
		    for (var s = this, i = 0; i < arguments.length; i++)
		        s = s.replace(new RegExp("\\{" + i + "\\}", "g"), arguments[i]);
		    return s;
		};
		// 格式化秒数到时间格式
		Number.prototype.formatTime = function() {
		    // 计算
		    var h = 0,
		        i = 0,
		        s = parseInt(this);
		    if (s > 60) {
		        i = parseInt(s / 60);
		        s = parseInt(s % 60);
		        if (i > 60) {
		            h = parseInt(i / 60);
		            i = parseInt(i % 60);
		        }
		    }
		    // 补零
		    var zero = function(v) {
		        return (v >> 0) < 10 ? "0" + v : v;
		    };
		    if (h > 0) {
		        return [h, zero(i), zero(s)].join(":");
		    } else if (i > 0) {
		        return [i, zero(s)].join(":");
		    } else {
		        return [s, "s"].join("");
		    }
		};

		// time 单位毫秒;
		function appShake(time) {
		    time = time || 1000; //默认1秒
		    plus.device.vibrate(time);
		}