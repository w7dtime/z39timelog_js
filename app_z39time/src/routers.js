/**
 * 整个app的路由设置
 */
const router = [{
    path: '/',
    name: "时间清单",
    redirect: '/m_task',
    component(resolve) {
      require.ensure(['./components/TimeIndex'], () => {
        resolve(require('./components/TimeIndex'));
      });
    },
    // component: TimeIndex,
    // redirect: "/m_task",
  },
  {
    path: '/n_tasktwo', //TimeTask
    name: "任务",
    redirect: "n_tasktwo/none",
    component(resolve) {
      require.ensure(['./components/TaskTwo'], () => {
        resolve(require('./components/TaskTwo'));
      });
    },
    children: [{
      path: '/n_tasktwo/none',
      name: '待办任务-模式2',
      component(resolve) {
        require.ensure(['./components/task/TimeTaskNone'], () => {
          resolve(require('./components/task/TimeTaskNone'));
        });
      }
    }, {
      path: '/n_tasktwo/today',
      name: '今日任务-模式2',
      component(resolve) {
        require.ensure(['./components/task/TimeTaskToday'], () => {
          resolve(require('./components/task/TimeTaskToday'));
        });
      }
    }, {
      path: '/n_tasktwo/project',
      name: '项目任务-模式2',
      component(resolve) {
        require.ensure(['./components/task/TimeTaskPro'], () => {
          resolve(require('./components/task/TimeTaskPro'));
        });
      }
    }, {
      path: '/n_tasktwo/habit',
      name: '习惯养成-模式2',
      component(resolve) {
        require.ensure(['./components/life/Habit'], () => {
          resolve(require('./components/life/Habit'));
        });
      }
    }, {
      path: '/n_tasktwo/taskmd',
      name: '日报分析-模式2',
      component(resolve) {
        require.ensure(['./components/life/TimeTaskMD'], () => {
          resolve(require('./components/life/TimeTaskMD'));
        });
      }
    }]
  },
  {
    path: '/m_mine',
    name: "我的",
    component(resolve) {
      require.ensure(['./components/Mine'], () => {
        resolve(require('./components/Mine'));
      });
    }
  },
  {
    path: '/login',
    name: "登录",
    component(resolve) {
      require.ensure(['./components/Login'], () => {
        resolve(require('./components/Login'));
      });
    }
  },
  {
    path: '/bind',
    name: "绑定",
    component(resolve) {
      require.ensure(['./components/Bind'], () => {
        resolve(require('./components/Bind'));
      });
    }
  },
  {
    path: '/aboutUs',
    name: "关于我们",
    component(resolve) {
      require.ensure(['./components/AboutUs'], () => {
        resolve(require('./components/AboutUs'));
      });
    }
  },
  {
    path: '/suggest',
    name: "反馈建议",
    component(resolve) {
      require.ensure(['./components/Suggest'], () => {
        resolve(require('./components/Suggest'));
      });
    }
  },
  {
    path: '/cooperate',
    name: "合作",
    component(resolve) {
      require.ensure(['./components/Cooperate'], () => {
        resolve(require('./components/Cooperate'));
      });
    }
  },
  {
    path: '/n_startup',
    name: "启动",
    component(resolve) {
      require.ensure(['./components/Startup'], () => {
        resolve(require('./components/Startup'));
      });
    }
  },
  {
    path: '/n_guide',
    name: "引导",
    component(resolve) {
      require.ensure(['./components/Guide'], () => {
        resolve(require('./components/Guide'));
      });
    }
  },
  {
    path: '/guidepop',
    name: "引导窗",
    component(resolve) {
      require.ensure(['./components/GuidePop'], () => {
        resolve(require('./components/GuidePop'));
      });
    }
  },
  {
    path: '/contacts',
    name: "通讯录",
    component(resolve) {
      require.ensure(['./components/Contacts'], () => {
        resolve(require('./components/Contacts'));
      });
    }
  },
  {
    path: '/charge',
    name: "充值",
    component(resolve) {
      require.ensure(['./components/Charge'], () => {
        resolve(require('./components/Charge'));
      });
    }
  },
  {
    path: '/setting',
    name: "设置",
    component(resolve) {
      require.ensure(['./components/Setting'], () => {
        resolve(require('./components/Setting'));
      });
    }
  },
  {
    path: '/n_import',
    name: "导入",
    component(resolve) {
      require.ensure(['./components/ImportUser'], () => {
        resolve(require('./components/ImportUser'));
      });
    }
  },
  // {
  //   path: '/test',
  //   name: "测试",
  //   component(resolve) {
  //     require.ensure(['./components/Test'], () => {
  //       resolve(require('./components/Test'));
  //     });
  //   }
  // },
  {
    path: '/m_timeauto',
    redirect: "m_timeauto/habit",
    name: "数据",
    component(resolve) {
      require.ensure(['./components/TimeAuto'], () => {
        resolve(require('./components/TimeAuto'));
      });
    },
    children: [{
      path: '/m_timeauto/taskmd',
      name: '日报分析',
      component(resolve) {
        require.ensure(['./components/life/TimeTaskMD'], () => {
          resolve(require('./components/life/TimeTaskMD'));
        });
      }
    }, {
      path: '/m_timeauto/habit',
      name: '习惯养成',
      component(resolve) {
        require.ensure(['./components/life/Habit'], () => {
          resolve(require('./components/life/Habit'));
        });
      }
    }, {
      path: '/m_timeauto/move',
      name: '动作记录',
      component(resolve) {
        require.ensure(['./components/life/AutoMove'], () => {
          resolve(require('./components/life/AutoMove'));
        });
      }
    }, {
      path: '/m_timeauto/stand',
      name: '专注时长',
      component(resolve) {
        require.ensure(['./components/life/AutoStatic'], () => {
          resolve(require('./components/life/AutoStatic'));
        });
      }
    }, {
      path: '/m_timeauto/all',
      name: '历史动作',
      component(resolve) {
        require.ensure(['./components/life/AutoAll'], () => {
          resolve(require('./components/life/AutoAll'));
        });
      }
    }]
  },
  {
    path: '/m_task', //TimeTask
    name: "任务",
    redirect: "m_task/none",
    component(resolve) {
      require.ensure(['./components/TimeTask'], () => {
        resolve(require('./components/TimeTask'));
      });
    },
    children: [{
      path: '/m_task/taskone',
      name: '待办清单-模式1',
      component(resolve) {
        require.ensure(['./components/TaskSimple'], () => {
          resolve(require('./components/TaskSimple'));
        });
      }
    }, {
      path: '/m_task/none',
      name: '待办任务',
      component(resolve) {
        require.ensure(['./components/task/TimeTaskNone'], () => {
          resolve(require('./components/task/TimeTaskNone'));
        });
      }
    }, {
      path: '/m_task/today',
      name: '今日任务',
      component(resolve) {
        require.ensure(['./components/task/TimeTaskToday'], () => {
          resolve(require('./components/task/TimeTaskToday'));
        });
      }
    }, {
      path: '/m_task/project',
      name: '项目任务',
      component(resolve) {
        require.ensure(['./components/task/TimeTaskPro'], () => {
          resolve(require('./components/task/TimeTaskPro'));
        });
      }
    }]
  },
  {
    path: '/m_market',
    name: "筛选",
    // component: TimeMarket
    component(resolve) {
      require.ensure(['./components/TimeMarket'], () => {
        resolve(require('./components/TimeMarket'));
      });
    }
  },
  {
    path: '/taskDetail/:id',
    name: "任务详情",
    component(resolve) {
      require.ensure(['./components/TaskDetail'], () => {
        resolve(require('./components/TaskDetail'));
      });
    }
  },
  {
    path: '/video/:id',
    name: "视频详情",
    component(resolve) {
      require.ensure(['./components/index/IndexVideo'], () => {
        resolve(require('./components/index/IndexVideo'));
      });
    }
  },
  {
    path: '/tproject',
    name: "项目",
    component(resolve) {
      require.ensure(['./components/TimeProject'], () => {
        resolve(require('./components/TimeProject'));
      });
    }
  },
  {
    path: '/agreement',
    name: "注册协议",
    component(resolve) {
      require.ensure(['./view/Agreement'], () => {
        resolve(require('./view/Agreement'));
      });
    }
  }
]
export default router;
