var tasks = [
    { uid: 0, id: "", title: "学会拒绝：让猴子回到主人身上", desc: "", due: "", state: "", pid: "" }, { uid: 0, id: "", title: "要事第一：依次做重要紧急、重要不紧急、紧急不重要、不紧急不重要", desc: "", due: "", state: "", pid: "" }, { uid: 0, id: "", title: "准备工作：提前准备好'前置条件',让工作行如流水,不卡壳", desc: "", due: "today", need: 36000, state: "", pid: 3 }, { uid: 0, id: "", title: "每晚总结：确定自己什么地方做的好，提升做的不好地方", desc: "", due: "today", need: 36000, state: "", pid: 3 }, { uid: 0, id: "", title: "每晚计划：明确第二天任务，可以快速进入工作状态", desc: "", due: "2018-08-20", need: 36000, state: "", pid: 3 }, { uid: 0, id: "", title: "外包杂事：事情太多必然无法完成，最大化产生价值，外包简单杂事", desc: "", due: "2018-08-20", need: 36000, state: "", pid: 3 }, { uid: 0, id: "", title: "准时睡觉：恢复身体的最佳状态，为第二天最高效率做准备", desc: "", due: "2018-08-20", need: 36000, state: "", pid: 3 },
    { uid: 0, id: "", title: "梦想价值：实现梦想时的时薪", desc: "", due: "2018-08-20", need: 36000, state: "", pid: 1 },
    { uid: 0, id: "", title: "时薪计算公式：工资/天数/工作小时(含加班和路程)", desc: "", due: "2018-08-20", need: 36000, state: "", pid: 1 },
    { uid: 0, id: "", title: "统筹方法：简单事情用身体不同部位做多件事情.", desc: "", due: "2018-08-20", need: 36000, state: "", pid: 1 },
    { uid: 0, id: "", title: "高速路：合理安排路上行人，犹如开上高速路", desc: "", state: "", pid: 1 },
    { uid: 0, id: "", title: "习惯：自动化高效完成同类任务", desc: "", state: "", pid: 1 },
    { uid: 0, id: "", title: "掌控：管理好自己才能管理好团队，否则只会乱上加乱", desc: "", state: "", pid: 1 }, { uid: 0, id: "", title: "5秒钟记录灵感", desc: "", state: "", pid: 2 }, { uid: 0, id: "", title: "空闲时定时整理灵感", desc: "", state: "", pid: 2 }, { uid: 0, id: "", title: "去除与目标无关的灵感", desc: "", state: "", pid: 2 }, { uid: 0, id: "", title: "灵感转换成执行任务，并分配执行的先后顺序。", desc: "", state: "", pid: 2 }, { uid: 0, id: "", title: "不记录就忘记，不在为忘记了什么重要事情而烦恼。", desc: "", state: "", pid: 2 }, { uid: 0, id: "", title: "做最有价值的事，不重要的创意自然靠后，避免为各种想法挖井放弃。", desc: "", state: "", pid: 2 }, { uid: 0, id: "", title: "《高效能人士的7个习惯》", desc: "", state: "", pid: 4 }, { uid: 0, id: "", title: "《高效演讲》", desc: "", state: "", pid: 4 }, { uid: 0, id: "", title: "《如何阅读一本书》", desc: "", state: "", pid: 4 }, { uid: 0, id: "", title: "《如何高效学习》", desc: "", state: "", pid: 4 }, { uid: 0, id: "", title: "《如何打造个人品牌》", desc: "", state: "", pid: 4 }, { uid: 0, id: "", title: "《85个实用好学的幽默技巧》", desc: "", state: "", pid: 4 }, { uid: 0, id: "", title: "《七十二堂写作课》", desc: "", state: "", pid: 4 }, { uid: 0, id: "", title: "《如何打造个人品牌》", desc: "", state: "", pid: 4 }, { uid: 0, id: "", title: "《认同感：用故事包装事实的艺术》", desc: "", state: "", pid: 4 },
];

// 录音 title desc url duration
var audios = [];

// {uid:0,id:"",title:"",desc:"",due:"",state:"",pid:""},
//项目project 数据
var projects = [{ id: 1, title: "时间价值", desc: "", uid: 0 },
    { id: 2, title: "灵感创意", desc: "", uid: 0 },
    { id: 3, title: "梦想清单", desc: "", uid: 0 },
    { id: 4, title: "学习清单", desc: "", uid: 0 },
    { id: 5, title: "运动健康", desc: "", uid: 0 },
    { id: 6, title: "家庭事务", desc: "", uid: 0 },
    { id: 7, title: "旅行计划", desc: "", uid: 0 },
    { id: 8, title: "习惯养成", desc: "", uid: 0 },
];

var habits = [{ uid: 0, id: "", title: "习惯性格人生", desc: "", due: "", state: "", pid: "" }, //生活 习惯养成
    { uid: 0, id: "", title: "量变到质变[由量生变]", desc: "", due: "", state: "", pid: "" },
    { uid: 0, id: "", title: "相互督促", desc: "", due: "", state: "", pid: "" },
    { uid: 0, id: "", title: "伙伴的力量", desc: "", due: "", state: "", pid: "" },
    { uid: 0, id: "", title: "更多视角与方法", desc: "", due: "", state: "", pid: "" },
    { uid: 0, id: "", title: "仿效的方法", desc: "", due: "", state: "", pid: "" }
];
var actionMove = [
    // { uid: 0, id: "", title: "举手投足", desc: "", start: "", end: "", timeday: "today", typea: 1 }, //动作记录
    { uid: 0, id: "", title: "时间去哪儿", desc: "时间来这儿,时间清单就像一个助理,关心我的一切", start: "", end: "", timeday: "today", typea: 1 },
    { uid: 0, id: "", title: "起心动念", desc: "为什么会突然跑去杂事,忘记当下.原来是想起曾经遗忘的事情", start: "", end: "", timeday: "today", typea: 1 },
    { uid: 0, id: "", title: "生命小偷", desc: "每一秒钟被这些讨厌的事情占用,真是讨厌死了.", start: "", end: "", timeday: "today", typea: 1 },
    { uid: 0, id: "", title: "多动症", desc: "原本专注做一件事,我却在刷朋友圈这无关事情.跑来跑去.", start: "", end: "", timeday: "today", typea: 1 },
    // ];
    // var actionStatic = [
    { uid: 0, id: "", title: "自信积淀", desc: "我专注做一件事情的时长原来这么久,都废寝忘食了.", start: "", end: "", timeday: "today", typea: 2 }, //专注时长
    { uid: 0, id: "", title: "专注区段", desc: "原来这些时期间,同事朋友容易来找我", start: "", end: "", timeday: "today", typea: 2 },
    // { uid: 0, id: "", title: "定海神针", desc: "", start: "", end: "", timeday: "today", typea: 2 },
    // ];
    // var actionAll = [
    { uid: 0, id: "", title: "积少成多", desc: "", start: "", end: "", timeday: "", typea: 1 }, //周末待办
    { uid: 0, id: "", title: "延迟满足", desc: "", start: "", end: "", timeday: "", typea: 2 },
    { uid: 0, id: "", title: "放松有望", desc: "", start: "", end: "", timeday: "", typea: 2 },
    { uid: 0, id: "", title: "家庭相伴", desc: "", start: "", end: "", timeday: "", typea: 1 }
];

exports.tasks = tasks;
exports.audios = audios;
exports.projects = projects;
// exports.blogs = blogs;
exports.habits = habits;
exports.actionMove = actionMove;
// exports.actionStatic = actionStatic;
// exports.actionAll = actionAll;
// exports.weekEnd = weekEnd;