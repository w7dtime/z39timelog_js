//Finally 20174/14 //
(function(win, lib) {
  let doc = win.document
  let docEl = doc.documentElement

  let dpr = 1 // 物理像素与逻辑像素的对应关系
  let scale = 1 // css像素缩放比率
  // 设置viewport
  function setViewport() {
    if (navigator.userAgent.indexOf("iPad") == -1) return; //只修改ipad

    dpr = 1
    win.devicePixelRatioValue = dpr
    //win.devicePixelRatio = win.devicePixelRatio*win.devicePixelRatio
    scale = 1 / dpr
    let metaEl = doc.createElement('meta')
    metaEl.setAttribute('name', 'viewport')
    metaEl.setAttribute('content', 'initial-scale=' + scale + ', maximum-scale=' + scale + ', minimum-scale=' + scale + ', user-scalable=no')
    if (docEl.firstElementChild) {
      docEl.firstElementChild.appendChild(metaEl)
    } else {
      let wrap = doc.createElement('div')
      wrap.appendChild(metaEl)
      doc.write(wrap.innerHTML)
    }
  }
  setViewport()
  let newBase = 100

  function setRem() {
    let visualView = Math.min(docEl.getBoundingClientRect().width, lib.maxWidth) // visual viewport
    newBase = 100 * visualView / lib.desinWidth
    docEl.style.fontSize = newBase + 'px'
  }
  let tid
  lib.desinWidth = 640
  lib.baseFont = 18
  lib.maxWidth = 540
  lib.init = function() {
    if (navigator.userAgent.indexOf("iPad") == -1) return; //只修改ipad

    win.addEventListener('resize', function() {
      clearTimeout(tid)
      tid = setTimeout(setRem, 300)
    }, false)
    /*win.addEventListener('onorientationchange', function () {
        clearTimeout(tid)
        tid = setTimeout(setRem, 300)
    }, false)*/
    win.addEventListener('pageshow', function(e) {
      if (e.persisted) {
        clearTimeout(tid)
        tid = setTimeout(setRem, 300)
      }
    }, false)
    if (doc.readyState === 'complete') {
      doc.body.style.fontSize = lib.baseFont * dpr + 'px'
    } else {
      doc.addEventListener('DOMContentLoaded', function(e) {
        doc.body.style.fontSize = lib.baseFont * dpr + 'px'
      }, false)
    }
    setRem()
    docEl.setAttribute('data-dpr', dpr)
  }
})(window, window['adaptive'] || (window['adaptive'] = {}))


window['adaptive'].desinWidth = 750
window['adaptive'].baseFont = 18
window['adaptive'].maxWidth = 750
window['adaptive'].init()

export default window['adaptive']
