/**
 * @apiDefine z39tag
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39tag?id=:id 获取标签
 * @apiVersion 1.0.0
 * @apiName GetTag
 * @apiGroup z39tag
 * @apiPermission code_system
 *
 * @apiDescription 获取单个标签的详细信息.
 * 标签表 id taskid pid bid uid value key  
 *           任务id 项目id blogid
 *
 * @apiParam {Number} id 标签id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39tag?id4711
 *
 * 
 * @apiSuccess {Number}   id         tags_id
 * @apiSuccess {String}   key           唯一key,英文或拼音 .
 * @apiSuccess {String}   value    标签值
 * @apiSuccess {String}   name          标签中文名
 * @apiSuccess {Number}   id         tags_id
 * @apiSuccess {Number}   pid         project_id
 * @apiSuccess {Number}   bid         blog_id
 * @apiSuccess {Number}   taskid      taskid_id
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError TagNotFound   The <code>id</code> of the z39tag was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getTag() { return; }

/**
 * @api {post} /api/z39tag 添加标签
 * @apiVersion 1.0.0
 * @apiName PostTag
 * @apiGroup z39tag
 * @apiPermission none
 *
 * @apiDescription 给项目、任务添加标签. 
 * 
 * @apiParam {String}   key      标签英文key
 * @apiParam {String}   [value]    标签值
 * @apiParam {Number}   [taskid]         任务id . 4选其一
 * @apiParam {Number}   [pid]            项目id .
 * @apiParam {Number}   [bid]            blog表id .
 * @apiParam {Number}   [uid]            用户id .
 *
 * @apiSuccess {Number} id     新的标签id z39tag-ID.
 *
 * @apiUse z39tag
 */
function postTag() { return; }

/**
 * @api {put} /api/z39tag?id=:id 修改标签
 * @apiVersion 1.0.0
 * @apiName PutTag
 * @apiGroup z39tag
 * @apiPermission none
 *
 * @apiDescription 通常是修改标签的value,如修改{key:due,value:'2017-11-29'}
 *
 * @apiParam {Number}   id    标签id
 * 
 * @apiParam {String}   [key]      标签英文key
 * @apiParam {String}   [value]    标签值
 * @apiParam {Number}   [taskid]         任务id . 
 * @apiParam {Number}   [pid]            项目id .
 * @apiParam {Number}   [bid]            blog表id .
 *
 * @apiUse z39tag
 */
function putTag() { return; }

/**
 * @api {delete} /api/z39tag?id=:id 删除标签
 * @apiVersion 1.0.0
 * @apiName DeleteTag
 * @apiGroup z39tag
 * @apiPermission none
 *
 * @apiDescription 删除标签 
 *
 * @apiParam {Number}     id    标签id
 *
 * @apiUse z39tag
 */
function deleteTag() { return; }