/**
 * @apiDefine z39invite
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39invite?id=:id 获取邀请
 * @apiVersion 1.0.0
 * @apiName Getz39invite
 * @apiGroup z39invite
 * @apiPermission admin | user
 *
 * @apiDescription 获取单个邀请的详细信息.
 * 表结构 z39invite: id type xid iid pwd ext time
 *
 * 
 *   type + xid , type对应类型 1:gid类型-群邀请链接, 现在只有固定值1. 
 *    			  xid 是具体的id数值. gid数值 ~~后期可能扩展为好友邀请~~
     z39invite?type=1&xid=?? 获取群的邀请链接,主要用于成员获取群邀请链接和二维码
     z39invite?type=1&xid=??&refresh=1  刷新邀请链接,旧邀请链接失效.
     z39invite?type=1&xid=??&pwd=?? 成员确认加入该群. pwd 来源于上面生成的url中
     已注册用户点击该链接直接加入群. 
 * 上面获取到的邀请链接 http://www.7dtime.com?action=invite&xid=??&type=1&pwd=??&iid=??
 * 
 * @apiParam {Number} id 对象id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39invite/4711
 *
 * @apiSuccess {Number}   id            邀请id .
 * @apiSuccess {String}   title    标题
 * @apiSuccess {String}   desc     描述
 * @apiSuccess {Number}   uid      作者
 * @apiSuccess {String}   start    录音开始时间
 * @apiSuccess {String}   end      录音结束时间
 * @apiSuccess {String}   duration   录音文件时长
 * @apiSuccess {Number}   taskid    录音文件属于的任务id
 * @apiSuccess {Number}   pid       录音文件所属项目id,
 * @apiSuccess {String}   url      文件网络url地址
 * @apiSuccess {String}   file     文件在APP中的本地地址
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39inviteNotFound   The <code>id</code> of the z39invite was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39invite() { return; }