/**
 * @apiDefine z39audio
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39audio?id=:id 获取音频
 * @apiVersion 1.0.0
 * @apiName Getz39audio
 * @apiGroup z39audio
 * @apiPermission admin | user
 *
 * @apiDescription 获取单个音频的详细信息.
 * 表结构 z39audio: id title desc uid start end duration taskid pid url file
 * 部分字段暂未使用, pid taskid 
 * 如果参数包含id,则返回的是指定id 单个数据对象.
 * 如果不含参数id,则返回用户所有数据的数组.
 * 
 * @apiParam {Number} id 对象id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39audio?id=4711
 *
 * @apiSuccess {Number}   id            音频id .
 * @apiSuccess {String}   title    标题
 * @apiSuccess {String}   desc     描述
 * @apiSuccess {Number}   uid      作者
 * @apiSuccess {String}   start    录音开始时间
 * @apiSuccess {String}   end      录音结束时间
 * @apiSuccess {String}   duration   录音文件时长
 * @apiSuccess {Number}   taskid    录音文件属于的任务id
 * @apiSuccess {Number}   pid       录音文件所属项目id,
 * @apiSuccess {String}   url      文件网络url地址
 * @apiSuccess {String}   file     文件在APP中的本地地址
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39audioNotFound   The <code>id</code> of the z39audio was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39audio() { return; }

/**
 * @api {post} /api/z39audio 添加音频
 * @apiVersion 1.0.0
 * @apiName Postz39audio
 * @apiGroup z39audio
 * @apiPermission none
 *
 * @apiDescription In this case "apiErrorStructure" is defined and used.
 * Define blocks with params that will be used in several functions, so you dont have to rewrite them.
 *
 * @apiParam {String}   title    标题
 * @apiParam {String}   desc     描述
 * @apiParam {String}   start    录音开始时间
 * @apiParam {String}   end      录音结束时间
 * @apiParam {String}   duration   录音文件时长
 * @apiParam {Number}   [taskid]    录音文件属于的任务id
 * @apiParam {Number}   [pid]       录音文件所属项目id,
 * @apiParam {String}   [url]      文件网络url地址.
 *
 * @apiSuccess {Number} id     新插入数据id
 *
 * @apiUse z39audio
 */
function postz39audio() { return; }

/**
 * @api {put} /api/z39audio?id=:id 修改音频
 * @apiVersion 1.0.0
 * @apiName Putz39audio
 * @apiGroup z39audio
 * @apiPermission none
 *
 * @apiDescription 可能会修改标题和描述.
 *
 * @apiParam {Number} id     音频id
 * @apiParam {String}   [title]    标题
 * @apiParam {String}   [desc]     描述
 * @apiParam {Number}   [taskid]    录音文件属于的任务id
 * @apiParam {Number}   [pid]       录音文件所属项目id,
 * @apiParam {String}   [url]      文件网络url地址.
 * 
 *
 * @apiUse z39audio
 */
function putz39audio() { return; }

/**
 * @api {delete} /api/z39audio?id=:id 删除音频
 * @apiVersion 1.0.0
 * @apiName Deletez39audio
 * @apiGroup z39audio
 * @apiPermission none
 *
 * @apiDescription 删除音频,从数据库及硬盘中删除.
 *
 * @apiParam {Number}     id    音频id
 *
 * @apiUse z39audio
 */
function deletez39audio() { return; }