/**
 * @apiDefine z39tasks
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39tasks?key=:key 获取任务列表,获取任务详情在z39task接口
 * @apiVersion 1.0.0
 * @apiName GetTask
 * @apiGroup z39tasks
 * @apiPermission user
 *
 * @apiDescription 获取用户的任务列表,只显示一级任务, 后期会添加外包的二级任务. 
 * 接口调用示例
 * /api/z39tasks?key=due:2017-08|2017-03 
 * /api/z39tasks?key=due:20171120|20171121,done,summary,study
 * 
 * @apiParam {String}   [key]    需要查询的标签数据.数据格式如 
 * @apiParam {String}   [pid]    项目id
 * @apiParam {String}   [parid]  父任务id, 不传改参数就是获取一级任务.正常是数字id, 用''代替null, 在使用的时候转换成Null.
 * @apiParam {Boolean}   [equal]   false: 模糊查询  true 精确查询. 用在查询"日月年"的日期上.
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39tasks?task=none&week=1&due=2017-07-31&
 *
 * @apiSuccess {Object[]} tasks     收纳箱任务数组
 * @apiSuccess {Number}   tasks.id            任务id tasks-ID.
 * @apiSuccess {String}     tasks.title    任务标题
 * @apiSuccess {String}     tasks.desc          任务描述
 * @apiSuccess {String}   tasks.state     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiSuccess {Number}   tasks.parid       父任务id
 * @apiSuccess {Number}   tasks.uid      用户成员id ,没有则不返回.
 * @apiSuccess {Object[]} tasks.tags     标签数组
 * @apiSuccess {String}   tasks.tags.id  标签 id.
 * @apiSuccess {String}   tasks.tags.key  标签 别名唯一.
 * @apiSuccess {String}   tasks.tags.value 标签 值 .
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError TaskNotFound   The <code>id</code> of the Task was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39tasks() { return; }

/**
 * @api {post} /api/z39tasks 添加任务
 * @apiVersion 1.0.0
 * @apiName Postz39tasks
 * @apiGroup z39tasks
 * @apiPermission none
 *
 * @apiDescription 高级功能,主要是导入数据使用, 批量添加任务.
 *
 * @apiParam {Number}   [pid]       项目id
 * @apiParam {Object[]}   tasks    任务标题
 * @apiParam {String}     tasks.title    任务标题
 * @apiParam {String}     tasks.desc          任务描述
 * @apiParam {String} 	tasks.state     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiParam {Number}   [tasks.tasks]       父任务id
 * @apiParam {Object[]}   [tasks.tags]           唯一key,英文或拼音 .
 * @apiParam {String}   tasks.tags.key           唯一key,英文或拼音 .
 * @apiParam {String}   tasks.tags.value    标签值
 * @apiParam {String}   tasks.tags.name          标签中文名
 *
 * @apiSuccess {Number[]} id     新的任务ids 
 *
 * @apiUse z39tasks
 */
function postz39tasks() { return; }