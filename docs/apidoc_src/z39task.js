/**
 * @apiDefine z39task
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39task?id=:id 获取任务详情, 获取任务列表在z39tasks接口.
 * @apiVersion 1.0.0
 * @apiName GetTask
 * @apiGroup z39task
 * @apiPermission  user
 *
 * @apiDescription 获取单个任务的详细信息. 包含任务标签和子任务
 * 任务表 z39task: id title desc pid parid state uid
 *
 * @apiParam {Number} id 任务id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39task?id=4711
 *
 * @apiSuccess {Number}   id            任务id .
 * @apiSuccess {Number}   pid            属于的项目id .
 * @apiSuccess {String}     title    任务标题
 * @apiSuccess {String}     desc          任务描述
 * @apiSuccess {String} state     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiSuccess {Number}   parid       父任务id
 * @apiSuccess {Number}   uid      用户成员id
 * @apiSuccess {String}   mhead      用户成员头像member
 * @apiSuccess {Object[]} tags     标签数组
 * @apiSuccess {String}   tags.id  标签 id.
 * @apiSuccess {String}   tags.key  标签 别名唯一
 * @apiSuccess {String}   tags.name  标签 中文名
 * @apiSuccess {String}   tags.value 标签 值 
 * @apiSuccess {Object[]} [tasks]     收纳箱任务数组
 * @apiSuccess {Number}   tasks.id            任务id tasks-ID.
 * @apiSuccess {String}     tasks.title    任务标题
 * @apiSuccess {String}     tasks.desc          任务描述
 * @apiSuccess {String}   tasks.state     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiSuccess {Number}   tasks.parid       父任务id
 * @apiSuccess {Number}   tasks.uid      用户成员id ,没有则不返回.
 * @apiSuccess {Object[]} tasks.tags     标签数组
 * @apiSuccess {String}   tasks.tags.id  标签 id.
 * @apiSuccess {String}   tasks.tags.key  标签 别名唯一.
 * @apiSuccess {String}   tasks.tags.value 标签 值 .
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError TaskNotFound   The <code>id</code> of the z39task was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getTask() { return; }

/**
 * @api {post} /api/z39task 添加任务
 * @apiVersion 1.0.0
 * @apiName PostTask
 * @apiGroup z39task
 * @apiPermission none
 *
 * @apiDescription In this case "apiErrorStructure" is defined and used.
 * Define blocks with params that will be used in several functions, so you dont have to rewrite them.
 *
 * @apiParam {String}     title    任务标题
 * @apiParam {String}     [desc]          任务描述
 * @apiParam {String}   [state]     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiParam {Number}   [parid]       父任务id
 * @apiParam {Number}   [pid]      项目id
 *
 * @apiSuccess {Number} id     新的任务id Tasks-ID.
 *
 * @apiUse z39task
 */
function postTask() { return; }

/**
 * @api {put} /api/z39task?id=:id 修改任务
 * @apiVersion 1.0.0
 * @apiName PutTask
 * @apiGroup z39task
 * @apiPermission none
 *
 * @apiDescription 创建任务后就不能修改二级任务的父任务id,否则报错.
 *
 * @apiParam {Number}     id    任务id
 * @apiParam {String}     [desc]          任务描述
 * @apiParam {String}   [state]     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiParam {Number}   [parid]       父任务id
 * @apiParam {Number}   [pid]      项目id
 *
 * @apiUse z39task
 */
function putTask() { return; }

/**
 * @api {delete} /api/z39task?id=:id 删除任务
 * @apiVersion 1.0.0
 * @apiName DeleteTask
 * @apiGroup z39task
 * @apiPermission none
 *
 * @apiDescription 删除任务,数据库实际是对任务做取消状态标记. 数据并未真实删除.
 *
 * @apiParam {Number}     id    任务id
 *
 * @apiUse z39task
 */
function deleteTask() { return; }