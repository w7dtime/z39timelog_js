/**
 * @apiDefine z39project
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /api/z39project?id=:id 获取项目
 * @apiVersion 1.0.0
 * @apiName Getz39project
 * @apiGroup z39project
 * @apiPermission admin | user
 *
 * @apiDescription 获取项目详细信息.包含标签、成员数组 . 
 * 表字段 id key title desc state uid
 *
 * @apiParam {Number} [id] 项目id
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/api/z39project/4711
 *
 * @apiSuccess {Number}   id            任务id .
 * @apiSuccess {String}   [key]    项目唯一标识,高级功能导入导出数据使用. 
 * @apiSuccess {String}     title    任务标题
 * @apiSuccess {String}     desc          任务描述
 * @apiSuccess {String}   state    项目状态 "" "done" | "cancelled"
 * @apiSuccess {Object[]} tags     标签数组
 * @apiSuccess {String}   tags.id  标签 id.
 * @apiSuccess {String}   tags.key  标签 别名唯一
 * @apiSuccess {String}   tags.name  标签 中文名
 * @apiSuccess {String}   tags.value 标签 值 
 * @apiSuccess {Object[]} tasks     任务数组
 * @apiSuccess {Object[]} tagtmp     项目模板
 * @apiSuccess {Object[]} members     项目成员列表
 * @apiSuccess {Number}   members.fuid     群成员id
 * @apiSuccess {Number}   members.admin    null或9
 * @apiSuccess {String}   members.headimgurl      用户成员头像member
 * @apiSuccess {String}   members.username      用户成员昵称
 * 
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError z39projectNotFound   The <code>id</code> of the z39project was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getz39project() { return; }

/**
 * @api {post} /api/z39project 添加项目
 * @apiVersion 1.0.0
 * @apiName Postz39project
 * @apiGroup z39project
 * @apiPermission none
 *
 * @apiDescription 添加项目,后端会自动给项目添加管理员. 配套的是通过添加任务并分配给项目，通过邀请链接添加成员.
 * 表字段 id key title desc state uid
 * 简单用法和其他rest接口一样,只需要注意到字段名.
 *
 * @apiParam {String}     title    项目标题
 * @apiParam {String}     desc          项目描述
 * @apiParam {String} state     项目状态 "" "done" | "cancelled"
 * @apiParam {String}   [key]    项目唯一标识,高级功能导入导出数据使用. 
 * @apiParam {Object[]} [tags]     标签数组, 高级功能-导入项目使用时会用到此参数
 * @apiParam {String}   tags.key   标签 key.
 * @apiParam {String}   tags.value 标签 值 
 * @apiParam {Object[]} [tasks]     标签数组, 高级功能-导入项目使用时会用到此参数
 * @apiParam {string}  [tmp]     模板key数组的字符,和z39tagtmp.tmp 格式“dev,art,bug” 
 * @apiParam {Number}  [tmpid]     模板z39tagtmp id . 如何和tmp都没传递,会使用默认模板内容
 * 
 * @apiParam {Object[]}  members     标签数组
 *
 * @apiSuccess {Number} id     新的项目id z39projects-ID.
 *
 * @apiUse z39project
 */
function postz39project() { return; }

/**
 * @api {put} /api/z39project?id=:id 修改项目
 * @apiVersion 1.0.0
 * @apiName Putz39project
 * @apiGroup z39project
 * @apiPermission none
 *
 * @apiDescription 创建项目, 项目人员的添加删除放在member表中.
 *
 * @apiParam {Number}     id    项目id
 * @apiParam {String}     title    项目标题
 * @apiParam {String}     desc          项目描述
 * @apiParam {String}     [key]    项目唯一标识,高级功能导入导出数据使用. 
 * @apiParam {Number}   [uid]      创建者uid,只有创建者可以修改此uid,
 * @apiParam {String}     state     项目状态 "" "done" | "cancelled"
 *
 * @apiUse z39project
 */
function putz39project() { return; }

/**
 * @api {delete} /api/z39project?id=:id 删除项目
 * @apiVersion 1.0.0
 * @apiName Deletez39project
 * @apiGroup z39project
 * @apiPermission none
 *
 * @apiDescription 删除项目 ,数据库实际是对项目做取消状态标记.
 *
 * @apiParam {Number}     id    项目id
 *
 * @apiUse z39project
 */
function deletez39project() { return; }