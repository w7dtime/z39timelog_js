/**
 * @apiDefine z39time
 * @apiVersion 1.0.0
 *
 */

/**
 * @api {get} /apix/z39time/usertask 获取初始数据
 * @apiVersion 1.0.0
 * @apiName GetUserTask
 * @apiGroup z39time
 * @apiPermission admin | user
 *
 * @apiDescription 用户进入系统,返回首页任务. 未分类、今日、项目名.
 *  不传递uid,从session中获取. * 
 *
 * @apiExample 示例用法:
 * curl -i https://www.7dtime.com/apix/z39time/usertask
 *
 * @apiSuccess {Object[]} none     收纳箱任务数组
 * @apiSuccess {Number}   none.id            任务id none-ID.
 * @apiSuccess {String}     none.title    任务标题
 * @apiSuccess {String}     none.desc          任务描述
 * @apiSuccess {String}   none.status     任务状态 "" | due | started | toggle | check | "done" | "cancelled"
 * @apiSuccess {Number}   none.parid       父任务id
 * @apiSuccess {Number}   none.uid      用户成员id ,没有则不返回.
 * @apiSuccess {String}   none.mhead      用户成员头像member,没有则不返回.
 * @apiSuccess {Object[]} none.tags     标签数组
 * @apiSuccess {String}   none.tags.id  标签 id.
 * @apiSuccess {String}   none.tags.key  标签 别名唯一.
 * @apiSuccess {String}   none.tags.name  标签 中文名.
 * @apiSuccess {String}   none.tags.value 标签 值 . * 
 * @apiSuccess {Object[]} today     今日任务数组,内容和上面一样.
 * @apiSuccess {Object[]} projs     项目数组
 * @apiSuccess {Number}   projs.id  标签 id.
 * @apiSuccess {Number}   projs.title  标签 id.
 * @apiSuccess {Number}   projs.key  标签 id.
 * @apiSuccess {Number}   projs.desc  标签 id.
 * @apiSuccess {Object[]} projs.tags     标签数组
 * @apiSuccess {String}   projs.tags.id  标签 id.
 * @apiSuccess {String}   projs.tags.key  标签 别名唯一.
 * @apiSuccess {String}   projs.tags.name  标签 中文名.
 * @apiSuccess {String}   projs.tags.value 标签 值 .
 * @apiSuccess {Object[]} tagitems     所有标签说明数据
 * @apiSuccess {Number}   tagitems.id  标签 id.
 * @apiSuccess {String}   tagitems.key  标签 别名唯一.
 * @apiSuccess {String}   tagitems.name  标签 中文名.
 * @apiSuccess {Object[]} tagtmps     模板数组,tagtmp.tmp 格式“tagitemid,tagitemid” 参考数据为"1305,1201"  都是日目标中的属性编辑纵横数据. 通过tmp字符串获取对应数据.
 * @apiSuccess {Object[]} members     日列表中的成员属性.
 * 
 * @apiError NoAccessRight Only authenticated Admins can access the data.
 * @apiError UserTaskNotFound   The <code>id</code> of the UserTask was not found.
 *
 * @apiErrorExample Response (example):
 *     HTTP/1.1 401 Not Authenticated
 *     {
 *       "error": "NoAccessRight"
 *     }
 */
function getUserTask() { return; }