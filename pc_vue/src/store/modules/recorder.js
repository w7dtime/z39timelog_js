export default {
    state: {
        recordTask: null,
        recording: false,
        globalRecording: false
    },
    mutations: {
        setRecordTask(state, task){
            this.state.recordTask = task;
        },
        setRecording(state, recording){
            this.state.recording = recording;
        },
        setGlobalRecording(state, recording){
            this.state.globalRecording = recording;
        }
    },
    actions: {
        initRecordTask({commit}, task){
            commit('setRecordTask', task)
        },
        initRecording({commit}, r){
            commit('setRecording', r)
            commit('setGlobalRecording', false)
        },
        initGlobalRecording({commit}, r){
            commit('setGlobalRecording', r)
            commit('setRecording', false)
            commit('setRecordTask', null)
        }
    }
}