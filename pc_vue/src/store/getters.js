export default {
    recordingTask: state=>state.recorder.recordTask,
    recording: state=>state.recorder.recording,
    globalRecording: state=>state.recorder.globalRecording
}