/*
 * @Author: thinkido 4077725@qq.com
 * @Date: 2020-09-02 05:44:48
 * @LastEditors: thinkido 4077725@qq.com
 * @LastEditTime: 2022-09-11 15:44:34
 * @FilePath: \pc_vue\src\main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 引入jquery
import 'jquery'

import Vue from 'vue';
import ElementUI from 'element-ui';
import VueRouter from 'vue-router'
import _ from 'lodash'
Vue.prototype._ = _
import 'element-ui/lib/theme-chalk/index.css';
import App from './App';
import api from './api';
import rtcApi from './api/rtcApi';
Vue.prototype.rtcApi = rtcApi;

Vue.use(ElementUI);
Vue.use(VueRouter)

import Vuex from 'vuex'

Vue.use(Vuex)

import vuexI18n from 'vuex-i18n'
import store from './common/store';

Vue.use(vuexI18n.plugin, store)

import VueSocketio from 'vue-socket.io';
let token = api.token || localStorage.token || '' ;
Vue.use(VueSocketio, `${api.globalUrl}?token=${token}` );
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

import 'video.js/dist/video-js.css';
import 'vue-video-player/src/custom-theme.css';

//import {ToastPlugin } from 'vuex'
//Vue.use(ToastPlugin)
// 引用路由配置文件
import routes from './common/routes'
// 使用配置文件规则
const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {
  window.document.title = to.name + (!to.path.includes('zhihu') ? " -时间清单" : '');

  next()
})
import ga from 'vue-ga'
import ba from 'vue-ba'
if (process.env.NODE_ENV != "development") { //dev.todo8.cn开发统计
  ga(router, 'UA-40324677-6');
  Vue.use(ba, '0ea90a88c8bf02ff94aa301a276ff886')
} else { //todo8.cn 时间清单正式服
  Vue.use(ba, 'eee3e11d38889522643bfe1bb9f343f9') //统计开发数据
}
router.afterEach((to) => {
  ba.trackPageview(to.fullPath)
})
// 跑起来吧
window.app = new Vue({ // eslint-disable-line no-new
  router,
  store,
  el: '#app1',
  render: (h) => h(App)
})

window.timeout = function(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
