import {mapState} from 'vuex'
import api from '@/api'
import UploadImg from '@/libs/image-uploader.js'
export default {
  props: ['editorContent','pageType'],
  data () {
    return {
      editorMD: null
    }
  },
  computed: {
    ...mapState({})
  },
  watch: {
    'editorContent' () {
      this.init()
      this.resizeEditor()
    }
  },
  methods: {
    getContent () {
      return this.editorMD.getMarkdown()
    },
    resizeEditor () {
      $('#editormd').scrollTop(0)
      $('.CodeMirror-wrap').css({'width': '50%'})
      $('.editormd-preview').css({'width': '50%'})
    },
    init () {
      if (this.editorMD) {
        if (!this.editorContent) this.editorMD.clear()
        else this.editorMD.setMarkdown(this.editorContent)
        return
      }
      var self = this
      // eslint-disable-next-line no-undef
      this.editorMD = editormd({
        id: 'editormd',
        width: '100%',
        height: '100%',
        syncScrolling: 'single',
        // theme: 'default',
        // previewTheme: 'dark',
        // editorTheme: 'pastel-on-dark',
        path: '/static/lib/',
        htmlDecode: 'style,script,iframe|on*',
        markdown: this.editorContent,
        emoji: true,
        taskList: true,
        fragment: true, // 开启碎片图
        tocm: true, // Using [TOCM]
        tex: true, // 开启科学公式TeX语言支持，默认关闭
        flowChart: true, // 开启流程图支持，默认关闭
        sequenceDiagram: true, // 开启时序/序列图支持，默认关闭

        imageUpload: true,
        imageFormats: ['jpg', 'jpeg', 'gif', 'png', 'bmp'],
        imageUploadURL: `${api.globalUrl}/apix/z39base/uploadbase64`,
        uploadImgMaxSize: 3 * 1024 * 1024,
        uploadImgMaxLength: 5,
        uploadImgTimeout: 12 * 1000,

        videoUpload: true,
        videoFormats: ['mp4', 'avi', 'rmvb', 'wmv', "webm"],
        videoUploadURL: `${api.globalUrl}/apix/z39base/upload`,
        uploadVideoMaxSize: 50 * 1024 * 1024,
        uploadVideoMaxLength: 1,
        uploadVideoTimeout: 10 * 60 * 1000, // 10 min
//		autoFocus:self.pageType=='add'?false:true,
        crossDomainUpload: true,
        onlyTask: true,
        uploadCallbackURL: 'eval',
        mind: true,
        onload: function () {
          this.setMarkdown(self.editorContent)
          this.watch()
          // this.previewing()
        },
        onchange: function () {
        	  //内容改变的时候，通知父节点更新
        	  try{
        	  	self.$emit('blogChanged',this.getMarkdown())
        	  }catch(error){}
          self.resizeEditor()
		}
      })
      this.editorMD.uploadImg = new UploadImg(this.editorMD)
    },
    //ctrl+s保存内容
    saveContent(){
    		try{
    			this.$parent.$refs.taskDetailComponent.saveTaskContent()
    		}catch(err){
    			console.log(err)
    		}
    }
  },
  mounted () {
    let that = this
    let m = document.createElement('script')
    m.src = "/static/jslib/editormd.js"
    document.body.appendChild(m)
    m.onload = function () {
      that.init()
      console.log($('#edit'))
    }
    
  }
}
