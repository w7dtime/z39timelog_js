import Vue from 'vue';

// list with filters page
// import login from '../views/task/login';
// import task from '../views/task/task';
// import todayTask from '../views/task/todayTask';
// import weekTask from '../views/task/weekTask';
// import monthTask from '../views/task/monthTask';
// import yearTask from '../views/task/yearTask';
// import projectTask from '../views/task/projectTask';
// import oneTask from '../views/task/oneTask';

// //需求和建议
// import needAndAdvice from '../views/needAndAdvice/needAndAdvice';
// import needAndAdviceContain from '../views/needAndAdvice/contain';
// //总结
// import note from '../views/note/note';
// //软文
// import ruanwen from '../views/ruanwen/ruanwen';
// //市场
// import market from '../views/market/market';
// //档案库
// import archive from '../views/archive/archive';
// // 文档
// import blogIndex from '../views/blog/blogIndex.vue';
// import blog from '../views/blog/blog';

let routes = [
  //  {
  //    path: "/login",
  //    component: login,
  ////    redirect: "/login",
  //    name: '登录',
  //  }, //重定向
  {
    path: '/',
    redirect: "/todayTask"
  }, {
    path: '/task',
    component: resolve => {
      require(['../views/task/task.vue'], resolve)
    },
    name: '任务',
    redirect: "/todayTask",
    children: [{
        path: '/todayTask',
        meta: {
          title: '今日任务',
        },
        component: resolve => {
          require(['../views/task/todayTask.vue'], resolve)
        },
        name: '今日任务'
      },
      {
        path: '/weekTask',
        meta: {
          title: '周任务',
        },
        component: resolve => {
          require(['../views/task/weekTask.vue'], resolve)
        },
        name: '周任务'
      },
      {
        path: '/monthTask',
        meta: {
          title: '月任务',
        },
        component: resolve => {
          require(['../views/task/monthTask.vue'], resolve)
        },
        name: '月任务'
      },
      {
        path: '/yearTask',
        meta: {
          title: '年任务',
        },
        component: resolve => {
          require(['../views/task/yearTask.vue'], resolve)
        },
        name: '年任务'
      },
      {
        path: '/projectTask',
        meta: {
          title: '项目任务',
        },
        component: resolve => {
          require(['../views/task/projectTask.vue'], resolve)
        },
        name: '项目任务'
      },
      {
        path: '/projectTask/:pid',
        meta: {
          title: '指定项目任务',
        },
        component: resolve => {
          require(['../views/task/projectTask.vue'], resolve)
        },
        name: '指定项目任务'
      },
      // {
      //   path: '/projectTask/:pid/:part',
      //   name: '项目任务部门'
      // },
      {
        path: '/oneTask',
        meta: {
          title: '单任务',
        },
        component: resolve => {
          require(['../views/task/oneTask.vue'], resolve)
        },
        name: '单任务'
      },
    ]
  },
  {
    path: "/note",
    meta: {
      title: '总结',
    },
    component: resolve => {
      require(['../views/note/note.vue'], resolve)
    },
    name: '总结',
  }, {
    path: "/ruanwen",
    meta: {
      title: '软文',
    },
    component: resolve => {
      require(['../views/ruanwen/ruanwen.vue'], resolve)
    },
    name: '软文',
  }, {
    path: "/market",
    meta: {
      title: '市场',
    },
    component: resolve => {
      require(['../views/market/market.vue'], resolve)
    },
    name: '市场',
  },
  {
    path: '/needAndAdvice',
    component: resolve => {
      require(['../views/needAndAdvice/needAndAdvice.vue'], resolve)
    },
    name: '需求',
    redirect: "/advice",
    children: [{
      path: '/advice',
      meta: {
        title: '建议1',
      },
      component: resolve => {
        require(['../views/needAndAdvice/contain.vue'], resolve)
      },
      name: '建议1',
      props: { switchType: 1 }
    }, {
      path: '/need',
      meta: {
        title: '需求2',
      },
      name: '需求2',
      component: resolve => {
        require(['../views/needAndAdvice/contain.vue'], resolve)
      },
      props: { switchType: 2 }
    }]
  },
  {
    path: '/archive',
    meta: {
      title: '档案库',
    },
    name: '档案库',
    component: resolve => {
      require(['../views/archive/archive.vue'], resolve)
    }
  },
  {
    path: '/blogEdit',
    meta: {
      title: '文档编辑',
    },
    name: '文档编辑',
    component: resolve => {
      require(['../views/blog/blogEdit.vue'], resolve)
    }
  },
  {
    path: '/seo',
    component: resolve => {
      require(['../views/seo/bdrank.vue'], resolve)
    },
    name: '百度排名'
  },
  {
    path: '/blog',
    meta: {
      title: '码客文档',
    },
    name: '码客文档',
    component: resolve => {
      require(['../views/blog/blogIndex.vue'], resolve)
    }
  },
  // {
  //   path: '/mind',
  //   meta: { title: '思维导图',    },
  //   name: '思维导图',
  //   component: resolve => {
  //     require(['../views/mind.vue'], resolve)
  //   }
  // },
];

export default routes;
