function isCollectTask (task) {
  var isCollect = false
  if (task.tags && task.tags.length > 0) {
    var tags = task.tags
    for (var i = 0; i < tags.length; i++) {
      if (tags[i].key === 'save') {
        isCollect = true
        break
      }
    }
  }

  return isCollect
}

var testEditor
var isShowMarkdown = false
import api from '../../api'
import loginDialog from '../../components/all/login.vue'
import headerNav from '../../components/all/header.vue'
import '../../../static/z39pc/js/jquery-2.1.0.js'
import { getDayMD } from '../../../static/jslib/parsetlog_common.js'
import { mixinPc } from '../../common/mixinPc'
import draggable from 'vuedraggable'
// import UploadImg from '@/libs/image-uploader.js'
import vEditor from '../../components/vditor/index.vue'

$.ajaxSetup({
  xhrFields: {
    withCredentials: true
  }
})

export default {
  name: 'dateLine',
  data () {
    return {
      apiHost: 'http://d7game.free.ngrok.cc',
      currentTab: 'first',
      day: '',
      month: '',
      week: '',
      searchTxt: '',
      token: '',
      compacted: false,
      weekTaskList: [],
      searchTaskList: [],
      collectedTaskList: [],
      cartTaskList: [],
      currentTasks: [],
      selectedTasks: [],
      templateData: '',
      blogContent: ''
    }
  },
  components: {
    headerNav,
    loginDialog,
    vEditor,
    draggable
  },
  mixins: [mixinPc],
  computed: {
  },
  watch: {
  },
  methods: {
    showLoginDialog () {
      this.$refs.loginDialog.loginDialogVisible = true
    },
    checkLogin () {
      api.checkLogin({}).then(data => {
        if (data.errno !== 0) {
          this.$store.commit({ type: 'reset' })
          localStorage.removeItem('timeUserInfo')
          localStorage.removeItem('tagTmpSortArr')
          this.timeUserInfo = ''
        }
      })
    },
    handleClick (tab, event) {
      var that = this
      switch (that.currentTab) {
        case 'first':
          that.currentTasks = that.weekTaskList
          break
        case 'second':
          that.currentTasks = that.searchTaskList
          break
        case 'third':
          that.currentTasks = that.collectedTaskList
          break
        case 'fourth':
          that.currentTasks = that.cartTaskList
          break
        default:
          break
      }
    },
    resizeEditor (e) {
      $('#layout').resize('100%')
      if (isShowMarkdown) {
        testEditor.previewing()
        testEditor.resize($('#layout').width() + 275 + 'px', '994px')
        $('.CodeMirror-wrap').css({ 'width': ($('#layout').width() + 275) / 2 + 'px' })
        $('.editormd-preview').css({ 'width': ($('#layout').width() + 275) / 2 + 'px' })
      } else if (document.body.clientWidth < 1220) {
        isShowMarkdown = true
        testEditor.resize('100%', '994px')
      }
      this.closeOpen(e)
    },
    closeOpen (e) {
      var $target = $(e.target)
      var $taskWrap = $('.task-wrap')
      var $layout = $('#layout')
      if (!$taskWrap.hasClass('hidden')) {
        $taskWrap.addClass('hidden')
        $layout.css({ 'margin-left': '320px' })
        let $t = $target[0].tagName === 'A' ? $target.find('i') : $target
        $t.removeClass('el-icon-d-arrow-left').addClass('el-icon-d-arrow-right')
      } else {
        $taskWrap.removeClass('hidden')
        $layout.css({ 'margin-left': '630px' })
        let $t = $target[0].tagName === 'A' ? $target.find('i') : $target
        $t.removeClass('el-icon-d-arrow-right').addClass('el-icon-d-arrow-left')
      }
    },
    putTasks (e) {
      var that = this
      var ts = that.selectedTasks
      console.log('selectedTasks:', ts)

      var md = getDayMD([{ name: '', desc: '', tasks: ts }], false)
      testEditor.setMarkdown(md)
      this.resizeEditor(e)
    },
    /**
     * 保存博客内容
     */
    saveBlog () {
      let that = this
      this.blogContent = this.$refs.vEditor ? this.$refs.vEditor.getContent() : ''
      var $html = $('.markdown-body')
      var $h1 = $html.find('h1')
      if ($h1.length === 0) {
        $h1 = $html.find('h3')
      }
      var btitle = $h1[0] ? $h1[0].textContent : ''
      var $blogDesc = $html.find('p')[0]
      var bdesc = $blogDesc ? $blogDesc.textContent.substring(0, 50) : ''
      if (!(btitle && btitle.trim()) || !(bdesc && bdesc.trim())) {
        return this.$message.error('请填写博客标题与内容')
      }
      api.blogAdd({
        type: 5,
        title: btitle,
        desc: bdesc,
        content: this.blogContent,
        url: ''
      }).then(function (res) {
        if (typeof res === 'string') {
          res = JSON.parse(res)
        }
        if (res.errno === 0) {
          that.$message('保存成功')
        } else {
          that.$message('保存失败:' + res.errmsg)
        }
      })
    },
    login (loginInfo) {
      var that = this
      return new Promise((resolve, reject) => {
        // 调用登录接口
        $.ajax({
          url: that.apiHost + '/center/public/loginmob',
          type: 'POST',
          contentType: 'application/json',
          dataType: 'json',
          data: JSON.stringify(loginInfo),
          success: function (data) {
            that.token = data.data.token
            $.ajaxSetup({
              headers: {
                Cookie: 'thinkjs=' + data.data.thinkjs
              }
            })
            resolve(true)
          }
        })
      })
    },
    getWeekTasks () {
      var that = this
      var date = new Date(that.day)
      var y = date.getFullYear()
      var m = date.getMonth() + 1
      var day = [y, m < 10 ? '0' + m : m].join('-')
      api.tasksGet({ 'key': 'due:' + day }).then(function (data) {
        var d = data.data
        if (!d.tasks || d.tasks.length === 0) {
          that.$message('未找到相关任务')
        }
        that.weekTaskList = d.tasks || []
        that.currentTasks = that.weekTaskList
        var ctasks = that.weekTaskList.filter(function (task) { return isCollectTask(task) })
        that.collectedTaskList = ctasks
      })
    },
    searchTasks () {
      var that = this
      api.taskGet({ content: that.searchTxt }).then(function (data) {
        console.log('searchTasks', data)
        var d = data.data
        if (!d || d.length === 0) {
          that.$message('未找到相关任务')
        }
        that.searchTaskList = d || []
        that.currentTasks = that.searchTaskList
        var ctasks = that.searchTaskList.filter(function (task) { return isCollectTask(task) })
        that.collectedTaskList = ctasks
      })
    },
    getBlog (bid) {
      var that = this
      $.ajax({
        url: that.apiHost + '/api/z39blog?id=' + bid,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
          console.log('getBlog', data)
        }
      })
    },
    loadPic: function (index, desc) {
      this.templateData = desc.title + desc.desc
    }
  },
  mounted () {
    console.log('mounted')
    var that = this
    this.checkLogin()

    // this.initEditor()

    var taskSrc = $('#src-task-list')[0]
    var taskSel = $('#sel-task-list')[0]

    taskSrc.ondragstart = function (e) {
      console.log(e)
      var transfer = e.dataTransfer
      transfer.effectAllowed = 'copy'
      transfer.setData('text/plain', e.target.getAttribute('data-index'))
      console.log('data-index=', e.target.getAttribute('data-index'))

      console.log('dragstart')
      $('.sel-task-wrap').removeClass('hidden')
      $('#layout').css({ 'margin-left': '630px' })

      return true
    }
    taskSel.ondragenter = function (e) {
      e.preventDefault()
      e.stopPropagation()
      console.log('dragenter')
      return false
    }
    taskSel.ondragover = function (e) {
      e.preventDefault()
      e.stopPropagation()
      console.log('dragiver')
      return false
    }
    taskSel.ondrop = function (e) {
      e.preventDefault()
      e.stopPropagation()
      console.log('drop')
      var transfer = e.dataTransfer
      transfer.dropEffect = 'move'
      var i = parseInt(transfer.getData('text/plain'))
      console.log('i=', i)

      that.selectedTasks.push(that.currentTasks[i])
      that.currentTasks.splice(i, 1)

      return false
    }

    taskSrc.ondragend = function (e) {
      e.preventDefault()
      e.stopPropagation()
      console.log('dragend')
      return false
    }

    taskSel.ondragstart = function (e) {
      console.log(e)
      var transfer = e.dataTransfer
      transfer.effectAllowed = 'copy'
      transfer.setData('text/plain', e.target.getAttribute('data-index'))
      console.log('data-index=', e.target.getAttribute('data-index'))

      console.log('dragstart')
      return true
    }
    taskSrc.ondragenter = function (e) {
      e.preventDefault()
      e.stopPropagation()
      console.log('dragenter')
      return false
    }
    taskSrc.ondragover = function (e) {
      e.preventDefault()
      e.stopPropagation()
      console.log('dragiver')
      return false
    }
    taskSrc.ondrop = function (e) {
      e.preventDefault()
      e.stopPropagation()
      console.log('drop')
      var transfer = e.dataTransfer
      transfer.dropEffect = 'move'
      var i = parseInt(transfer.getData('text/plain'))
      console.log('i=', i)

      that.currentTasks.push(that.selectedTasks[i])
      that.selectedTasks.splice(i, 1)

      return false
    }
    taskSel.ondragend = function (e) {
      e.preventDefault()
      e.stopPropagation()
      console.log('dragend')
      return false
    }

    $('#close-sel').click(function () {
      $('.sel-task-wrap').addClass('hidden')
      $('#layout').css({ 'margin-left': '320px' })
    })
  }
}
