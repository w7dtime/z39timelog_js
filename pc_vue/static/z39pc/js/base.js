
let base={};
base.Sortable=function(containerArr) {
	containerArr.forEach(function(item,isAll) {
		var operateObj=$(item )[0]
		if(isAll){
			operateObj=$(item)
		}
		Sortable.create($(item )[0], {
			ghostClass: ".dragArea",
			scrollSensitivity: 50,
			animation: 150,
			group: "task",
			onStart: function(){
				
			},
			onEnd: function(){}
		});
	})

}
function padLeftZero (str) {
   return ('00' + str).substr(str.length)
}
base.formatDate=function (date, fmt) {
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    let o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'h+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds()
    };
    for (let k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
            let str = o[k] + '';
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str));
        }
    }
    return fmt;
};
export default base;