# 时间清单-vue
z39timelog_js_os\myPlugins\elementUI 目录里的文件覆盖 z39timelog_js_os\pc_vue\node_modules\element-ui 中的文件 ， 复制，粘贴、覆盖。
```video
<video id="my-video" class="video-js" controls preload="none" width="100%" poster="http://cdn.7dtime.com/time_tlog/2019-03-30_6pc_vuexmqd.jpg" data-setup='{"aspectRatio":"16:9"}'>
 <source src="http://cdn.7dtime.com/time_tlog/2019-03-30_6pc_vuexmqd.mp4" type="video/mp4" >
 <p class="vjs-no-js">
 To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
```

> A Vue.js project


## Build Setup

``` bash
# install dependencies
npm install
node Version: 12.22.2

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
